﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

namespace Flipbook
{
	public class FlipbookPlayer : MonoBehaviour
	{
		public List<ClipBoundingData> BBoxes { get; protected set; }
		public List<ClipBSphereData> BSpheres { get; protected set; }
		public List<ClipBLineData> BLines { get; protected set; }

#if false
		// directory of sprites that are involved with the flipbook animation
		public Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
		public Dictionary<string, Texture2D> normalMaps = new Dictionary<string, Texture2D>();

		protected GameObject spriteLayer;
		protected SpriteRenderer spriteRenderer;
		protected CelLayer celLayer;

		private CelLayers layers;
		private bool bTriggered = false;


		/// <summary>
		/// assets that this system requires.
		/// </summary>
		public TextAsset spriteClipsFile;
		public TextAsset spriteAtlasFile;
		public Texture2D spriteMapFile;
		public Texture2D spriteNormalMapFile;

		public string spritePath;
		public string clipsFile;
		public string atlasFile;
		public string atlasImage;


		#region Properties
		public bool DebugDraw { get; set; }
		public Color DebugColor { get; set; }

		public string ClipsFile
		{
			get { return clipsFile; }
			set { clipsFile = value; }
		}
		public string AtlasFile
		{
			get { return atlasFile; }
			set { atlasFile = value; }
		}
		public string AtlasImage
		{
			get { return atlasImage; }
			set { atlasImage = value; }
		}

		public string SpritePath
		{
			get { return spritePath; }
			set { spritePath = value; }
		}


		public List<ClipBoundingData> BBoxes { get; protected set; }
		public List<ClipBSphereData> BSpheres { get; protected set; }
		public List<ClipBLineData> BLines { get; protected set; }
		#endregion

		Clips clipContainer;
		ClipData curClip;
		string curClipName;


		float elapsedTime = 0;
		float playbackSpeed = 1.0f;
		int frame = 0;
		float pixelsToUnits = 100f;




		/// <summary>
		/// Setup the the conversion of pixels to units, for bounding
		/// boxes calculations.  Default is 100
		/// </summary>
		/// <param name="unit">conversion value</param>
		public void SetPixelsToUnits(float unit)
		{
			pixelsToUnits = unit;
		}


		public void InitSprites()
		{
			string fname = spritePath + "/" + atlasImage;
			spriteMapFile = Resources.Load<Texture2D>(fname);				// load the sprite map

			if (spriteMapFile != null)
			{
				spriteClipsFile = Resources.Load<TextAsset>(spritePath + "/" + clipsFile);
				spriteAtlasFile = Resources.Load<TextAsset>(spritePath + "/" + atlasFile);

				loadAtlas();
				loadClips();
			}
		}


		/// <summary>
		/// play the requested animation on the screen
		/// </summary>
		/// <param name="name">name of the animation</param>
		public void Play(string name)
		{
			// check if we are already playing the current animation
			if (name != curClipName)
			{
				ClipData clipdata;
				clipContainer.ClipMap.TryGetValue(name, out clipdata);

				if (clipdata != null)
				{
					bTriggered = false;
					frame = 0;
					curClipName = name;
					curClip = clipdata;
					playbackSpeed = 1f / (float)clipdata.Fps;
					elapsedTime = 0f;
				}
				else
				{
					Debug.LogError("failed to find clip: " + name);
				}
			}
		}

		/// <summary>
		/// Setup the flip value for the sprites.
		/// </summary>
		/// <param name="flip"></param>
		public void Flip(Vector3 flip)
		{
			transform.localScale = flip;
		}


		#region Unity Specific
		void Awake()
		{
			Transform tran = transform.FindChild("cel");
			if (tran)
			{
				spriteLayer = tran.gameObject;
				layers = spriteLayer.GetComponent<CelLayers>();
				celLayer = spriteLayer.GetComponent<CelLayer>();
			}
		}


		/// <summary>
		/// update the animation
		/// </summary>
		void Update()
		{
			elapsedTime += Time.deltaTime;

			if (curClip != null)
			{
				if (elapsedTime > playbackSpeed)
				{
					elapsedTime -= playbackSpeed;
					bTriggered = false;
					frame++;
				}

				if (frame >= curClip.NumFrames)
				{
					if (curClip.Playback == ePlayback.PlaybackLoop)
					{
						frame = 0;
					}
					else
					{
						frame = curClip.NumFrames - 1;
						gameObject.SendMessage("onClipDone", curClipName);
						curClipName = "clipDone";
					}
				}

				var frameData = curClip.Frames[frame];

				if (frameData != null)
				{
					if (frameData.BBoxes.Count != 0)
						BBoxes = frameData.BBoxes;
					else
						BBoxes = null;

					if (frameData.BSpheres.Count != 0)
						BSpheres = frameData.BSpheres;
					else
						BSpheres = null;

					if (frameData.BLines.Count != 0)
						BLines = frameData.BLines;
					else
						BLines = null;
				}

				// check if the events have been triggered for this
				// frame or not.
				if (bTriggered == false)
				{
					// no .. not yet, so parse through the triggers and call
					// the tags.
					foreach (ClipTriggerData triggers in frameData.Triggers)
					{
						gameObject.SendMessage(triggers.Tag, GetInstanceID());
					}

					// set that the events have been triggered for this frame
					bTriggered = true;
				}

				Sprite sprite;
				//			Texture2D normal;

				// loop through all of the sprites ...
				for (int i = 0; i < frameData.Sprites.Count; i++)
				{
					var spriteData = frameData.Sprites[i];

					sprites.TryGetValue(spriteData.Name, out sprite);
					//				normalMaps.TryGetValue(spriteData.Name, out normal);

					if (sprite != null)
					{
						Vector3 offset = new Vector3(spriteData.XOffset, -spriteData.YOffset, 0);
						//					spriteLayer.transform.localPosition = offset / pixelsToUnits;
						Vector3 localPos = offset / pixelsToUnits;

						if (layers != null)
							layers.SetSprite(i, sprite, spriteData.Name, localPos);
						else
						{
							if (celLayer != null)
							{
								celLayer.currentSprite = sprite;
								celLayer.currentName = spriteData.Name;
							}
						}

					}
				}

			}
			transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);

		}


		void LateUpdate()
		{
			Camera mainCamera = Camera.main;
			transform.LookAt(mainCamera.transform.position, Vector3.up);
			transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);
		}



		/// <summary>
		/// Draw the gizmos on the associated BaseGameEntity
		/// </summary>
		void OnDrawGizmos()
		{
			DebugDrawBounds();
		}
		#endregion

		#region Debug
		/// <summary>
		/// Draw the debug bounds for the frame
		/// </summary>
		private void DebugDrawBounds()
		{
			if (DebugDraw == true)
			{
				if (BBoxes != null)
				{
					foreach (ClipBoundingData bound in BBoxes)
					{
						Bounds bd = ConvertBoundingData(transform, bound);
						Gizmos.color = DebugColor;
						Gizmos.DrawWireCube(bd.center, bd.extents);
					}
				}

				if (BSpheres != null)
				{
					foreach(ClipBSphereData sphere in BSpheres)
					{
						Vector3 pos = ConvertSphereData(transform, sphere);
						Gizmos.color = DebugColor;
						Gizmos.DrawSphere(pos, sphere.Radius);
					}
				}

				if (BLines != null)
				{

				}
			}
		}


	
		/// <summary>
		/// Convert the bounding data to Unity3d bounds class.
		/// </summary>
		/// <param name="t">transform of the object</param>
		/// <param name="bd">bound data</param>
		/// <returns></returns>
		private Bounds ConvertBoundingData(Transform t, ClipBoundingData bd)
		{
			Bounds bounds = new Bounds(Vector3.zero, new Vector3((bd.Width * 2f) / pixelsToUnits, (bd.Height * 2f) / pixelsToUnits, 2f));

			Vector3 pos = t.position;

			Vector3 offset;
			if (t.localScale.x < 0)
			{
				offset = new Vector3(-bd.XOffset, -bd.YOffset, 0);
				offset.x -= bd.Width / 2f;
				offset.y -= bd.Height / 2f;
			}
			else
			{
				offset = new Vector3(bd.XOffset, -bd.YOffset, 0);
				offset.x += bd.Width / 2f;
				offset.y -= bd.Height / 2f;
			}
			offset = offset / pixelsToUnits;

			pos += offset;
			bounds.center = pos;

			return bounds;
		}



		private Vector3 ConvertSphereData(Transform t, ClipBSphereData sp)
		{
			Vector3 pos = t.position;
			Vector3 offset;

			if (t.localScale.x < 0)
			{
				offset = new Vector3(-sp.XOffset, -sp.YOffset, 0);
			}
			else
			{
				offset = new Vector3(sp.XOffset, -sp.YOffset, 0);
			}
			offset = offset / pixelsToUnits;
			pos += offset;

			return pos;
		}


		/// <summary>
		/// Convert the line data to world coordinates
		/// </summary>
		/// <param name="t">tranform position</param>
		/// <param name="line">the line structure</param>
		/// <param name="a">starting point of the line</param>
		/// <param name="b">end point of the line</param>
		private void ConvertLineData(Transform t, ClipBLineData line, out Vector3 a, out Vector3 b)
		{
			Vector3 pos = t.position;

			if (t.localScale.x < 0)
			{
				a = new Vector3(-line.XStart, -line.YStart, 0);
				b = new Vector3(-line.XEnd, -line.YEnd, 0);
			}
			else
			{
				a = new Vector3(line.XStart, -line.YStart, 0);
				b = new Vector3(line.XEnd, -line.YEnd, 0);
			}
			a = a / pixelsToUnits;
			b = b / pixelsToUnits;
			
			a += pos;
			b += pos;
		}
		#endregion

		#region Serializer
		/// <summary>
		/// load the atlas image and create sprites using the atlas.
		/// </summary>
		protected void loadAtlas()
		{
			TextAsset xmlData = spriteAtlasFile;

			if (xmlData == null)
			{
				Debug.LogError("ERROR: no sprite atlas file specified");
			}
			else
			{
				var serializer = new XmlSerializer(typeof(CelMap));
				var reader = new StringReader(xmlData.text);
				var celContainer = serializer.Deserialize(reader) as CelMap;

				foreach (CelData atlas in celContainer.Atlas)
				{
					if (sprites.ContainsKey(atlas.Name) == false)
					{
						int yoffset = spriteMapFile.height - atlas.Height - atlas.YOffset;
						Sprite spriteObject = Sprite.Create(spriteMapFile,
															new Rect(atlas.XOffset, yoffset, atlas.Width, atlas.Height),
															new Vector2(0.0f, 1.0f), pixelsToUnits);
						sprites.Add(atlas.Name, spriteObject);

						if (spriteNormalMapFile != null)
						{
							Sprite normalObject = Sprite.Create(spriteNormalMapFile,
																new Rect(atlas.XOffset, yoffset, atlas.Width, atlas.Height),
																new Vector2(0.0f, 1.0f), pixelsToUnits);

							normalMaps.Add(atlas.Name, normalObject.texture);
						}
					}
					else
					{
						Debug.LogWarning("clip name conflict - (" + atlas.Name + ")");
					}
				}
			}
		}



		/// <summary>
		/// load the animation clips into the character
		/// </summary>
		protected void loadClips()
		{
			if (spriteClipsFile == null)
			{
				Debug.Log("ERROR: no sprite Clips file specified");
			}
			else
			{
				var serializer = new XmlSerializer(typeof(Clips));
				var reader = new StringReader(spriteClipsFile.text);
				if (serializer != null)
				{
					clipContainer = serializer.Deserialize(reader) as Clips;

					clipContainer.InitClips();
					Play("Ready");
				}
			}
		}
		#endregion
#endif
	}
}
