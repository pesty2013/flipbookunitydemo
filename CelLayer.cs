﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Flipbook
{
	public class CelLayer : MonoBehaviour
	{
		public Sprite currentSprite;
		public SpriteRenderer spriteRenderer;
		public string currentName;

		// Use this for initialization
		void Start()
		{
			GetComponent<Renderer>().shadowCastingMode = ShadowCastingMode.On;
			GetComponent<Renderer>().receiveShadows = true;

			transform.localScale = Vector3.one;
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;

			spriteRenderer = GetComponent<SpriteRenderer>();
		}

		// Update is called once per frame
		void Update()
		{
			if (currentSprite != null)
			{
				spriteRenderer.sortingOrder = 0;
				spriteRenderer.sprite = currentSprite;
			}
		}
	}
}
