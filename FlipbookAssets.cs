﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using Flipbook;
using System.Xml;
using System;
using System.Reflection;



public class FlipbookAssets
{
	#region Data Members
	public Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
	public Dictionary<string, Texture2D> normalMaps = new Dictionary<string, Texture2D>();
	public Dictionary<string, ClipData> clipsMap = new Dictionary<string, ClipData>();

	private Texture2D spriteMapFile;
	private Texture2D spriteNormalMapFile;
	private Clips clipContainer;

	string clipsFile;
	string atlasFile;
	string spritePath;
	string atlasImage;

	float pixelsToUnits = 100f;			// default value
	#endregion


	#region Playback variables
#if false
	float elapsedTime = 0f;
	private ClipData curClip;				// current clipdata being played
	private string curClipName;				// current clip name
	private float playbackSpeed = 1.0f;		// current playback speed
	private int frame = 0;					// current frame
	private bool bTriggered = false;		// any events triggered
	private int UID;
	private GameObject go;
	private SpriteRenderer Renderer;
#endif
	#endregion

	#region Properties
#if false
	public GameObject GO
	{
		get { return go; }
		set 
		{ 
			go = value;
			InstanceID = go.GetInstanceID();
		}
	}

	public int InstanceID
	{
		get { return UID; }
		set { UID = value; }
	}
#endif



	public Texture2D AtlasMapImage
	{
		get { return spriteMapFile; }
		set { spriteMapFile = value; }
	}

	public Clips ClipsContainer
	{
		get { return clipContainer; }
		set { clipContainer = value; }
	}


	public string ClipsFile
	{
		get { return clipsFile; }
		set { clipsFile = value; }
	}

	public string AtlasFile
	{
		get { return atlasFile; }
		set { atlasFile = value; }
	}

	public string AtlasImage
	{
		get { return atlasImage; }
		set { atlasImage = value; }
	}

	public string SpritePath
	{
		get { return spritePath; }
		set { spritePath = value; }
	}

	public float PixelsToUnits
	{
		get { return pixelsToUnits; }
		set { pixelsToUnits = value; }
	}
	#endregion

	#region Constructors
	public FlipbookAssets()
	{

	}


	public FlipbookAssets(string clipFile, string atlasFile, string atlasImage, string path, float pixelsToUnits)
	{
		ClipsFile = clipFile;
		AtlasFile = atlasFile;
		AtlasImage = atlasImage;
		SpritePath = path;
		PixelsToUnits = pixelsToUnits;

		InitAssets();
	}
	#endregion



	/// <summary>
	/// Load the assets for flipbook given the attributes and
	/// using the resources that are available.
	/// </summary>
	/// <param name="attr"></param>
	public void InitAssets()
	{
		string imageFile = SpritePath + "/" + AtlasImage;
		spriteMapFile = Resources.Load<Texture2D>(imageFile);               // load the sprite map

		string atlasFilename = SpritePath + "/" + AtlasFile;
		loadAtlas(atlasFilename);

		string clipsFilename = SpritePath + "/" + ClipsFile;
		loadClips(clipsFilename);
	}



	/// <summary>
	/// load the atlas image and create sprites using the atlas.
	/// </summary>
	protected void loadAtlas(string fname)
	{
		TextAsset xmlData = Resources.Load<TextAsset>(fname);

		if (xmlData == null)
		{
			Debug.LogError("ERROR: no sprite atlas file specified");
		}
		else
		{
			var serializer = new XmlSerializer(typeof(CelMap));
			var reader = new StringReader(xmlData.text);
			var celContainer = serializer.Deserialize(reader) as CelMap;

			foreach (CelData atlas in celContainer.Atlas)
			{
				if (sprites.ContainsKey(atlas.Name) == false)
				{
					int yoffset = spriteMapFile.height - atlas.Height - atlas.YOffset;
					Sprite spriteObject = Sprite.Create(spriteMapFile,
														new Rect(atlas.XOffset, yoffset, atlas.Width, atlas.Height),
														new Vector2(0.0f, 1.0f), pixelsToUnits);
					sprites.Add(atlas.Name, spriteObject);

					if (spriteNormalMapFile != null)
					{
						Sprite normalObject = Sprite.Create(spriteNormalMapFile,
															new Rect(atlas.XOffset, yoffset, atlas.Width, atlas.Height),
															new Vector2(0.0f, 1.0f), pixelsToUnits);

						normalMaps.Add(atlas.Name, normalObject.texture);
					}
				}
				else
				{
					Debug.LogWarning("clip name conflict - (" + atlas.Name + ")");
				}
			}
		}
	}



	/// <summary>
	/// Load the flipbook clip assets and store them.
	/// </summary>
	protected void loadClips(string fname)
	{
		TextAsset spriteClipsFile = Resources.Load<TextAsset>(fname);

		if (spriteClipsFile == null)
		{
			Debug.Log("ERROR: no sprite Clips file specified");
		}
		else
		{
			var serializer = new XmlSerializer(typeof(Clips));
			var reader = new StringReader(spriteClipsFile.text);
			if (serializer != null)
			{
				clipContainer = serializer.Deserialize(reader) as Clips;
				clipContainer.InitClips();

				// populate the dictionary for a faster lookup
				foreach (ClipData clipdata in clipContainer.ClipList)
				{
					clipsMap.Add(clipdata.Name, clipdata);
				}
			}
		}
	}
}
