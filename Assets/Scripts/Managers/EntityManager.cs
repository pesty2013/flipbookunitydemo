using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Entity manager
/// 
/// This singleton instance manages all of the created entities and stores
/// them with a unique ID value that is used for lookup.  The Dictionary
/// collection contains all of the references and game objects.
/// </summary>
public class EntityManager
{
	#region Singleton Entity Manager
	private static EntityManager instance = new EntityManager();

	EntityManager()
	{
		entityMap = new Dictionary<int, GameObject>();
		entityDestroyList = new List<int>();
	}

	public static EntityManager Instance
	{
		get { return instance; }
	}
	#endregion

	#region Data Members
	/// <summary>
	/// Collection of all of the entities within the world
	/// </summary>
	private IDictionary<int, GameObject> entityMap;
	public IDictionary<int, GameObject> EntityMap
	{
		get { return entityMap; }
	}


	/// <summary>
	/// Collection of InstanceID that get flagged to be destroyed
	/// </summary>
	private List<int> entityDestroyList;
	public List<int> EntityDestroyList
	{
		get { return entityDestroyList; }
	}
	#endregion

	#region Public Methods
	/// <summary>
	/// Registers the entity into the new map
	/// </summary>
	/// <param name='newentity'>
	/// Newentity.
	/// </param>
	public void Register(GameObject go)
	{
		if (go != null)
		{
			int id = go.GetInstanceID();
			EntityMap.Add(id, go);
		}
	}
	
	

	
	/// <summary>
	/// Destroys all of the entities that are in the game.
	/// 
	/// This is a destructive method and will remove everything
	/// that has been added to the world through the EntityManager
	/// </summary>
	public void DestroyAll()
	{
		UpdateDestoryList();
				
		// loop through and destroy all of the entities ...
		foreach (KeyValuePair<int,GameObject> go in entityMap)
		{
			DestroyGameObject(go.Value);
		}
		entityMap.Clear();
	}

	
	
	/// <summary>
	/// Flag a game object to be destroyed and removed from the 
	/// game.  This is done in the update feature.
	/// </summary>
	/// <param name='id'>id of gameobject</param>
	public void FlagToDestroy(int id)
	{
		entityDestroyList.Add(id);
	}
	#endregion

	#region Private Methods
	/// <summary>
	/// Destroys the requested game object and removes it
	/// from the registry.
	/// </summary>
	/// <param name='go'>GameObject to destroy</param>
	private void DestroyGameObject(GameObject go)
	{
		if (go != null)
		{
			int id = go.GetInstanceID();
			bool bRemoved = EntityMap.Remove(id);
			GameObject.Destroy(go);
		}
	}

	/// <summary>
	/// destroy all of the requested entities
	/// </summary>
	private void UpdateDestoryList()
	{
		foreach (int id in entityDestroyList)
		{
			GameObject ob = null;
			EntityMap.TryGetValue(id, out ob);
			if (ob != null)
			{
				DestroyGameObject(ob);
			}
		}
		entityDestroyList.Clear();
	}
	#endregion
}


