﻿using UnityEngine;


/// <summary>
/// The Game manager acts like a central communication script for all of
/// the different parts of the game.  In a way, it is the main glue that 
/// holds together the various components that make up the game.
/// </summary>
public class GameManager
{
	private static GameManager instance = new GameManager();

	GameManager() {
	}

	public static GameManager Instance {
		get { return instance; }
	}


	/// <summary>
	/// Pause the game ... this will setup the Physics and
	/// deltaTime to move at normal speed, or slow speed.
	/// </summary>
	/// <param name="paused">boolean to say pause or not</param>
	public void Pause(bool paused)
	{
		Time.timeScale = (paused == false ? 0.0f : 1.0f);
	}
}


