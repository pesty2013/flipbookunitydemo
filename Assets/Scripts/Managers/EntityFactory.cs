﻿using System;
using System.Collections.Generic;
using System.Reflection;


/// <summary>
/// All the different types of entities that can exist within
/// the world.
/// </summary>
public enum EntityType
{
	Knight,
	BlackKnight,
	RedKnight,
	BlueKnight,
	GreenKnight,
	YellowKnight
};



/// <summary>
/// To allow us to configure each ensemble type by the entity
/// type, we use the Builder pattern.  A builder basically is 
/// a class that encapsulates several initializations,
/// configuration, or data access operations for building a
/// class type.  So, for a particular group of inherited members
/// of the BaseEntity, we will need a different builder to 
/// configure each type's attributes in different ways.
/// </summary>
/// <see cref="Design Patterns: Builder Pattern, written by Christopher Lasater"/>
/// <see cref="http://stackoverflow.com/questions/5262693/c-sharp-using-activator-createinstance"/>
public sealed class EntityFactory
{
	
	private static Dictionary<string, Func<BaseEntity>> InstanceCreateCache = new Dictionary<string, Func<BaseEntity>>();
	
	/// <summary>
	/// Our actual factory create method receives as an input
	/// parameter the EntityType enum string and returns the class
	/// instance from the loaded collection.  If no values exists
	/// we load and create a entity
	/// </summary>
	/// <param name="entityType">the name of the class to create</param>
	/// <returns>the new entity</returns>
	
	public BaseEntity CreateBaseGameEntity(string entityName, string builderName)
	{
		if (!InstanceCreateCache.ContainsKey(entityName))
		{
			Load(entityName, builderName);
		}
		
		BaseEntity entity = InstanceCreateCache[entityName].Invoke();
		
		return entity;
	}
	
	
	
	/// <summary>
	/// Init the properties of the entity.
	/// </summary>
	/// <param name="entity">reference to the entity</param>
	/// <param name="attributes">attributes associated with the entity</param>
	private void InitProperties(ref BaseEntity entity, KeyValuePair<string, string> entry)
	{
		Type type = entity.GetType();
		PropertyInfo prop = type.GetProperty(entry.Key);
		
		if (prop != null)
		{
			prop.SetValue(entity, Convert.ChangeType(entry.Value, prop.PropertyType), null);
		}
	}
	
	
	
	/// <summary>
	/// Init the properties of the enity.
	/// </summary>
	/// <param name="entity">reference to the entity</param>
	/// <param name="attributes">attributes associated with the entity</param>
	private void InitPlanner(ref BaseEntity entity, KeyValuePair<string, string> entry)
	{
#if WINDOWS
		Type type = entity.GetType();
		
		PropertyInfo prop = type.GetProperty(entry.Key);
		if (prop != null)
		{
			if (entry.Key == "Planner")
			{
				object behavior = typeof(EntityManager).InvokeMember(entry.Value,
				                                               BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod,
				                                               null, null, new object[] { });
				
				if (behavior != null)
				{
					prop.SetValue(entity, Convert.ChangeType(behavior, prop.PropertyType), null);
				}
			}
		}
#endif
	}
	
	
	/// <summary>
	/// create a unique entity using the given attributes passed in the 
	/// dictionary collection.  The dictionary contains all of the 
	/// unique properties given to a class.
	/// 
	/// 
	/// </summary>
	/// <param name="attributes"></param>
	/// <returns></returns>
	public BaseEntity CreateEntity(Dictionary<string, string> attributes)
	{
		BaseEntity entity = null;
		
		
		if (attributes != null)
		{
			if (attributes.ContainsKey("type") && attributes.ContainsKey("builder"))
			{
				entity = CreateBaseGameEntity(attributes["type"], attributes["builder"]);
				
				if (entity != null)
				{
					Type type = entity.GetType();
					
					if (type != null)
					{
						// loop through all of the attributes
						foreach (KeyValuePair<string, string> entry in attributes)
						{
							PropertyInfo prop = type.GetProperty(entry.Key);
							
							switch (entry.Key)
							{
							case "Planner":
								InitPlanner(ref entity, entry);
								break;
								
							default:
								InitProperties(ref entity, entry);
								break;
							}
						}
					}
				}
			}
		}
		return entity;
	}
	


	
	/// <summary>
	/// The load method constructs all the types registered with
	/// the Register() method we saw above and loads them into
	/// the collection.  The factory performs the actual loading
	/// of the registered types using reflection on the class
	/// type from the values in the registry dictionary object.  
	/// The loaded type is defined by stating an expected base 
	/// type of BaseEntity, and the implementation class is
	/// loaded by its type with the EntityType enum as the 
	/// dictionary key.
	/// </summary>
	private void Load(string entityName, string builderName)
	{

		if (!InstanceCreateCache.ContainsKey(entityName))
		{
			// get the type (several ways exist, this is the easy one)
//			Type type = TypeDelegator.GetType("TreeSharpTest." + entityName);
			Type type = TypeDelegator.GetType(entityName);

			// works with public instance/static method
			MethodInfo mi = type.GetMethod("Create");
			
			// the "magic", turn it into a delegate
			var createInstanceDelegate = (Func<BaseEntity>)Delegate.CreateDelegate(typeof(Func<BaseEntity>), mi);
			
			// store for future reference
			InstanceCreateCache.Add(entityName, createInstanceDelegate);
		}
	}
}

