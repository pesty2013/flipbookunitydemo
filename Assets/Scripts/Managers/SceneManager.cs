﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System;
using System.Reflection;


public class SceneManager : MonoBehaviour 
{

	private bool sd, hd, normal, desert, english, danish;


	// Use this for initialization
	void Start () 
	{
		string moePath = Path.Combine(Application.dataPath, "Resources/moeDesc.xml");
		LevelDirector(moePath);
	}


	void OnGUI()
	{
		// GUI Padding
		GUILayout.Space (20);
		GUILayout.BeginHorizontal ();
		GUILayout.Space (20);
		GUILayout.BeginVertical ();

		// GUI Buttons
		// New Line - Get HD/SD
		GUILayout.BeginHorizontal ();
		// Display the choice
		GUILayout.Toggle (sd, "");
		// Get player choice
		if (GUILayout.Button ("Load SD")) {sd = true; hd= false; }
		// Display the choice
		GUILayout.Toggle (hd, "");
		// Get player choice
		if (GUILayout.Button ("Load HD")) {sd = false; hd = true; }
		GUILayout.EndHorizontal ();
			
		// New Line - Get Normal/Desert
		GUILayout.BeginHorizontal ();
		// Display the choice
		GUILayout.Toggle (normal, "");
		// Get player choice
		if (GUILayout.Button ("Normal")) {normal = true; desert= false; }
		// Display the choice
		GUILayout.Toggle (desert, "");
		// Get player choice
		if (GUILayout.Button ("Desert")) {normal = false; desert = true; }
		GUILayout.EndHorizontal ();
			
		// New Line - Get Language
		GUILayout.BeginHorizontal ();
		// Display the choice
		GUILayout.Toggle (english, "");
		// Get player choice
		if (GUILayout.Button ("English")) {english = true; danish = false; }
		// Display the choice
		GUILayout.Toggle (danish, "");
		// Get player choice
		if (GUILayout.Button ("Danish")) {english = false; danish = true; }
		GUILayout.EndHorizontal ();

		// GUI Padding
		GUILayout.Space (15);

		// Load the Scene
		if (GUILayout.Button ("Reset Player"))
		{
			// Remove the buttons
			GameObject player = GameObject.FindGameObjectWithTag("Player");
			if (player != null)
				player.SendMessage("OnResetMoe");
		}


		if (GUILayout.Button("Push Player"))
		{
			// Remove the buttons
			GameObject player = GameObject.FindGameObjectWithTag("Player");
			if (player != null)
			{
				Rigidbody rBody = player.GetComponent<Rigidbody>();

				Vector3 push = new Vector3(0, 10f, 0);
				push *= rBody.mass;
				rBody.AddForce(push, ForceMode.Impulse);
			}
		}


		if (GUILayout.Button("TestSuccess"))
		{
			FlipbookColliders collider = new FlipbookColliders();
			bool ret = collider.TestSuccess();

		}

		// End GUI Padding
		GUILayout.EndVertical ();
		GUILayout.EndHorizontal ();

	}
	/// <summary>
	/// Fleshes out the properties that were stored in the XML and copies
	/// them into the component that is within Unity3D
	/// </summary>
	/// <param name="compObj">target component</param>
	/// <param name="propInfo">property information within component</param>
	/// <param name="value">value to be stored in the component's property</param>
	private void PropertyBuilder(Component compObj, PropertyInfo propInfo, object value)
	{
//		Debug.Assert(value != null);
//		Debug.Assert(propInfo != null);

		if (propInfo != null && value != null)
		{
			if (propInfo.PropertyType == typeof(UnityEngine.Color))
			{
				Color color = new Color(1, 0, 0);
				propInfo.SetValue(compObj, color, null);
			}
			else if (propInfo.PropertyType == typeof(UnityEngine.Vector3))
			{
				var position = value.ToString();
				var elements = position.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
				var vec = new Vector3(float.Parse(elements[0]), float.Parse(elements[1]), float.Parse(elements[2]));
				propInfo.SetValue(compObj, vec, null);
			}
			else
			{
				var writeValue = Convert.ChangeType(value, propInfo.PropertyType);
				propInfo.SetValue(compObj, writeValue, null);
			}
		}
	}



	/// <summary>
	/// Parse out the EntityDescComponentDesc[] array of components.  creates
	/// the components, and then populates the properties over to the component
	/// and stores adds them to the gameobject
	/// </summary>
	/// <param name="instance">game object instance</param>
	/// <param name="desc">array of components and their description</param>
	private void ComponentBuilder(GameObject instance, EntitiesEntityDescComponentDesc[] desc)
	{
		foreach (EntitiesEntityDescComponentDesc compDesc in desc)
		{
			if (compDesc.TypeName == "Unity")
			{
				// special case for Unity transform parts
#if false
				foreach (EntitiesEntityDescComponentDescPropDesc prop in compDesc.PropDesc)
				{
					Type propType = prop.GetType();

					PropertyInfo[] properties = propType.GetProperties();
					foreach (PropertyInfo property in properties)
					{
						var value = propType.GetProperty(property.Name).GetValue(prop, null);

						PropertyInfo propInfo = compType.GetProperty(property.Name);
						if (propInfo != null)
						{
							PropertyBuilder(compObj, propInfo, value);
						}
					}
				}
#endif
			}
			else
			{
				Type compType = Type.GetType(compDesc.TypeName);

				if (compType != null)
				{
					Component compObj = instance.AddComponent(compType);
					foreach (EntitiesEntityDescComponentDescPropDesc prop in compDesc.PropDesc)
					{
						Type propType = prop.GetType();

						PropertyInfo[] properties = propType.GetProperties();
						foreach (PropertyInfo property in properties)
						{
							var value = propType.GetProperty(property.Name).GetValue(prop, null);

							PropertyInfo propInfo = compType.GetProperty(property.Name);
							if (propInfo != null)
							{
								PropertyBuilder(compObj, propInfo, value);
							}
						}
					}
				}
			}
		}
	}



	/// <summary>
	/// Create a flipbook character
	/// </summary>
	/// <param name="instance"></param>
	/// <param name="desc"></param>
	private void FlipbookBuilder(GameObject instance, EntitiesEntityDescFlipbookDesc desc)
	{
		FlipbookPlayer fp = instance.AddComponent<FlipbookPlayer>();
		Component moveComp = instance.GetComponent<SimpleMove>();
		if (moveComp == null)
		{
			Debug.Log("missing SimpleMove component required for flipbook");
		}

		var child = new GameObject("Cel");

		SpriteRenderer render = child.AddComponent<SpriteRenderer>();
		Renderer spriteRender = render.GetComponent<Renderer>();
		Material mat = Resources.Load<Material>("SpriteMaterial");
		spriteRender.material = mat;
		child.transform.SetParent(instance.transform, false);

		var scale = child.transform.localScale;
		scale.x = -1;
		child.transform.transform.localScale = scale;


		float pixels = 100f;
		bool bTry = float.TryParse(desc.PixelsToUnits, out pixels);
#if DEBUG
		if (bTry == false)
			Debug.LogWarning("failed to parse PixelsToUnits: default to 100");
#endif

		// here we need to initialize the flipbook player, which loads the assets
		// and creates the needed stuff to be copied to the player.
		FlipbookAssets fa = new FlipbookAssets(desc.ClipsFile,
												desc.AtlasFile,
												desc.AtlasImage,
												desc.SpritePath,
												pixels);

		fp.ClipContainer = fa.ClipsContainer;       // copy the clips container
		fp.SpriteAtlasImage = fa.AtlasMapImage;     // copy the sprite image
		fp.Clips = fa.clipsMap;                     // copy clips map
		fp.SpriteContainer = fa.sprites;

		fp.InitPlayer();

		Debug.Log("Created assets");
	}



	private void LevelDirector(string levelDesc)
	{
		XmlSerializer ser = new XmlSerializer(typeof(Entities));
		Entities entities;
		GameObject entity = null;


		using (XmlReader reader = XmlReader.Create(levelDesc))
		{
			entities = (Entities)ser.Deserialize(reader);

			if (entities != null)
			{
				foreach (EntitiesEntityDesc desc in entities.Items)
				{
					entity = EntityBuilder(desc);
					if (entity != null)
					{
						entity.SendMessage("OnEnable");					// enable it ...
						EntityManager.Instance.Register(entity);		// then register it
					}
				}
			}
		}
	}



	private GameObject EntityBuilder(EntitiesEntityDesc en)
	{
		// create the base game object first
		GameObject entity = (GameObject)Instantiate(Resources.Load(en.Prefab));
		ComponentBuilder(entity, en.ComponentDesc);

		// do we have a flipbook component
		if (en.FlipbookDesc != null)
		{
			FlipbookBuilder(entity, en.FlipbookDesc[0]);
		}
		return entity;
	}







	/// <summary>
	/// The director creates a new instance of an entity as 
	/// described by the EntityDesc that is passed through 
	/// to the director.
	/// 
	/// The EntityDesc is an XML description 
	/// </summary>
	/// <param name="moe"></param>
	/// <returns></returns>
	private GameObject EntityDirector(string moe)
	{
		XmlSerializer ser = new XmlSerializer(typeof(EntitiesEntityDesc));
		EntitiesEntityDesc entityDesc;
		GameObject entity = null;

		using (XmlReader reader = XmlReader.Create(moe))
		{
			entityDesc = (EntitiesEntityDesc)ser.Deserialize(reader);

			if (entityDesc != null)
			{
				// create the base game object first
				entity = (GameObject)Instantiate(Resources.Load(entityDesc.Prefab));
				ComponentBuilder(entity, entityDesc.ComponentDesc);

				// do we have a flipbook component
				if (entityDesc.FlipbookDesc != null)
				{
					FlipbookBuilder(entity, entityDesc.FlipbookDesc[0]);
				}
			}
		}
		entity.SendMessage("OnEnable");

		return entity;
	}


	private TextAsset LoadLevelParam(string levelname)
	{
		TextAsset textAsset = (TextAsset)Resources.Load(levelname);
		return textAsset;
	}





	/// <summary>
	/// Parse the flipbook attributes
	/// </summary>
	/// <param name="reader"></param>
	/// <param name="endNode"></param>
	/// <returns></returns>
	private Dictionary<string, string> ParseEntityAttributes(ref XmlReader reader, string endNode)
	{
		Dictionary<string, string> attributes = new Dictionary<string, string>();

		string key = "unknown";
		string value = "unknown";

		do
		{
			switch (reader.NodeType)
			{
				case XmlNodeType.Element:
					key = reader.Name;
					break;


				case XmlNodeType.Text:
					Debug.Log(reader.Value);
					value = reader.Value;
					break;


				case XmlNodeType.EndElement:
					if (reader.Name == endNode)
					{
						Debug.Log("<" + reader.Name + ">");
						return attributes;
					}
					else
					{
						attributes.Add(key, value);
						Debug.Log("<" + reader.Name + ">");

						// reset the key & value after we have added it 
						// to the attributes table.
						key = "unknown";
						value = "unknown";
					}
					break;
			}
		} while (reader.Read());

		return null;
	}
}
