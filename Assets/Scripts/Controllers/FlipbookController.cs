﻿using UnityEngine;

public class FlipbookController : BaseController
{
	public float inputDelay = 0.1f;         // delay for movement, wait till input exceeds 0.1
	public float distToGrounded = 0.1f;
	public LayerMask ground;
	public float downAccel = 3f;


	private Rigidbody rBody;
	private CapsuleCollider capsule;

	private bool grounded = false;
	private Vector3 groundVelocity;
	private bool jumpFlag = false;




	public override bool Grounded()
	{
		return Physics.Raycast(transform.position, Vector3.down, distToGrounded, ground);
	}

	void Awake()
	{
		rBody = GetComponent<Rigidbody>();
		capsule = GetComponent<CapsuleCollider>();

		if (rBody == null)
		{
			Debug.LogError("The character needs a rigidbody");
		}
		rBody.freezeRotation = true;
		rBody.useGravity = true;
	}


	void FixedUpdate()
	{
		Run();
	}


	void Run()
	{
		Vector3 vel = moveDir;

		// move
		if (!Grounded())
		{
			vel.y -= downAccel;
		}

		// move
		rBody.velocity = vel;
	}

	public override void MoveUpdate()
	{

		moveDir = MoveDirection * MoveForce;
		base.MoveUpdate();
	}
}
