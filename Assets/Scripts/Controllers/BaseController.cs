﻿using UnityEngine;
using System.Collections;

public class BaseController : MonoBehaviour {

	[HideInInspector]
	public bool faceRight = true;
	public float moveForce = 20f;
	public float maxSpeed = 5f;

	protected Vector3 moveDir = Vector3.zero;
	public Vector3 MoveDirection { get; set; }

	public float MoveForce
	{
		get { return moveForce; }
		set { moveForce = value; }
	}

	public float MaxSpeed
	{
		get { return maxSpeed; }
		set { maxSpeed = value; }
	}


	public float walkSpeed = 3.0f;
	public float walkBackwardSpeed = 4.0f;
	public float runSpeed = 5f;
	public float runBackwardSpeed = 6.0f;
	public float sidestepSpeed = 8.0f;
	public float runSidestepSpeed = 12.0f;
	public float maxVelocityChange = 1f;

	// Air
	public float inAirControl = 0.1f;
	public float jumpHeight = 2.0f;

	// Can Flags
	public bool canRunSidestep = true;
	public bool canJump = true;
	public bool canRun = true;


	public float distToGrounded = 0.1f;
	public LayerMask ground;

	public virtual bool Grounded()
	{
		return Physics.Raycast(transform.position, Vector3.down, distToGrounded, 256);
	}


	public virtual void MoveUpdate()
	{
		float h = MoveDirection.x;

		// deal with flipping of the sprites
		if (h < 0 && !faceRight)
		{
			Flip();
		}

		else if (h > 0 && faceRight)
		{
			Flip();
		}
	}



	/// <summary>
	/// Flip the orientation of the sprite animation
	/// </summary>
	protected void Flip()
	{
		faceRight = !faceRight;

		// send a message that we have flipped
		gameObject.SendMessage("OnFlip");
	}
}
