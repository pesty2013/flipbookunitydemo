﻿using UnityEngine;
using System.Collections;

public class SimpleMove : BaseController {

	protected CharacterController ctrl;

	void Start()
	{
		ctrl = GetComponent<CharacterController>();

		if (ctrl == null)
		{
			Debug.LogError("CharacterController is missing");
			CapsuleCollider collider = gameObject.GetComponent<CapsuleCollider>();
			if (collider != null)
			{
				Destroy(collider);
			}

			ctrl = gameObject.AddComponent<CharacterController>();

			ctrl.center = new Vector3(0, 0.5f, 0);
			ctrl.radius = 0.2f;
			ctrl.height = 0.8f;
		}
	}


	public override void MoveUpdate()
	{
		moveDir = MoveDirection * MoveForce;
		base.MoveUpdate();
	}



	void FixedUpdate()
	{
		ctrl.SimpleMove(moveDir);
	}
}


