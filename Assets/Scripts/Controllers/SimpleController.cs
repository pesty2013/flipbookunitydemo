﻿using UnityEngine;
using System.Collections;

public class SimpleController : BaseController
{
	public float inputDelay = 0.1f;         // delay for movement, wait till input exceeds 0.1
	public float forwardVel = 12f;
	public float rotateVel = 100f;          // how fast to turn


	Quaternion targetRotation;
	Rigidbody rBody;
	float forwardInput, turnInput;

	public Quaternion TargetRotation
	{
		get { return targetRotation; }
	}


	void Start()
	{
		targetRotation = transform.rotation;
		rBody = GetComponent<Rigidbody>();
		if (rBody == null)
		{
			Debug.LogError("The character needs a rigidbody");
		}
		forwardInput = turnInput = 0;
	}


	void GetInput()
	{
		forwardInput = Input.GetAxis("Vertical");
		turnInput = Input.GetAxis("Horizontal");
	}


	void Update()
	{
		GetInput();
		Turn();
	}

	void FixedUpdate()
	{
		Run();
	}


	void Run()
	{
		if (Mathf.Abs(forwardInput) > inputDelay)
		{
			// move
			rBody.velocity = transform.forward * forwardInput * forwardVel;
		}
		else
		{
			// zero velocity to remove
			rBody.velocity = Vector3.zero;
		}
	}


	void Turn()
	{
		if (Mathf.Abs(turnInput) > inputDelay)
		{
			targetRotation *= Quaternion.AngleAxis(rotateVel * turnInput * Time.deltaTime, Vector3.up);
		}
		transform.rotation = targetRotation;
	}



}
