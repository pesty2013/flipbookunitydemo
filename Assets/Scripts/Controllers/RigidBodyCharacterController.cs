﻿using UnityEngine;


/// <summary>
/// A rigid body character controller
/// http://answers.unity3d.com/questions/8676/make-rigidbody-walk-like-character-controller.html
/// 
/// 
/// http://answers.unity3d.com/questions/25967/how-can-i-create-a-round-arc-physics-based-jump.html
/// 
/// http://answers.unity3d.com/questions/25967/how-can-i-create-a-round-arc-physics-based-jump.html
/// </summary>
public class RigidBodyCharacterController : BaseController
{
	public float inputDelay = 0.1f;         // delay for movement, wait till input exceeds 0.1
	public float downAccel = 3f;

	private Rigidbody rBody;
	private CapsuleCollider capsule;

	private bool grounded = false;
	private Vector3 groundVelocity;
	private bool jumpFlag = false;
	private bool jumping = false;

	public float initialAngle = 10;

	#region Unity
	void Awake()
	{
		rBody = GetComponent<Rigidbody>();
		capsule = GetComponent<CapsuleCollider>();

		if (rBody == null)
		{
			Debug.LogError("The character needs a rigidbody");
		}
		rBody.freezeRotation = true;
		rBody.useGravity = true;
	}


	void Update()
	{
		jumpFlag = Input.GetButton("Jump");
	}


	void FixedUpdate()
	{
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		var inputVector = new Vector3(h, 0, v);             // save new direction;

		grounded = Grounded();

		if (true)
		{
			if (grounded)
			{
				// jumping
				if (canJump && jumpFlag)
				{
					jumpFlag = false;
					InitPhysicsJump();
					jumping = true;

					//					rBody.velocity = new Vector3(rBody.velocity.x, 
					//												rBody.velocity.y + CalculateJumpVerticalSpeed(), 
					//												rBody.velocity.z);
				}
				else
				{
					var velocityChange = CalculateVelocityChange(inputVector);
					rBody.AddForce(velocityChange, ForceMode.Impulse);
				}

			}
			else
			{
//				var velocityChange = transform.TransformDirection(inputVector) * inAirControl;
//				rBody.AddForce(velocityChange, ForceMode.VelocityChange);
			}
		}
	}
	#endregion



	public override void MoveUpdate()
	{
		moveDir = MoveDirection * MoveForce;
		base.MoveUpdate();
	}


	// From the user input calculate using the set up speeds the velocity change


	/// <summary>
	/// Calculate a velocity change from an input parameter
	/// </summary>
	/// <param name="inputVector"></param>
	/// <returns></returns>
	private Vector3 CalculateVelocityChange(Vector3 inputVector)
	{
		// Calculate how fast we should be moving
		var relativeVelocity = transform.TransformDirection(inputVector);
		float speed;

		if (inputVector.z > 0)
		{
			speed = canRun ? runSpeed : walkSpeed; 
//			relativeVelocity.z *= (canRun && Input.GetButton("Sprint")) ? runSpeed : walkSpeed;
		}
		else
		{
			speed = canRun ? runSpeed : walkSpeed;
//			speed = canRun  ? runBackwardSpeed : walkBackwardSpeed;
//			relativeVelocity.z *= (canRun && Input.GetButton("Sprint")) ? runBackwardSpeed : walkBackwardSpeed;
		}
		relativeVelocity.z *= speed;
		relativeVelocity.x *= speed;
//		relativeVelocity.x *= (canRunSidestep && Input.GetButton("Sprint")) ? runSidestepSpeed : sidestepSpeed;

		// Calcualte the delta velocity
		var currRelativeVelocity = rBody.velocity - groundVelocity;
		var velocityChange = relativeVelocity - currRelativeVelocity;
		velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
		velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
		velocityChange.y = 0;

		velocityChange *= rBody.mass;
		return velocityChange;
	}


	/// <summary>
	/// This sets up a physics jump, by solving object's motion along the 
	/// two axes separately.  Given values, position of target (target), our
	/// position (Owner), and the angle that we want to jump at.  From this, 
	/// we know the planar distance between the object (no y-axis) and the
	/// offset on the y-axis between the two.
	/// 
	/// </summary>
	/// <seealso cref="http://forum.unity3d.com/threads/how-to-calculate-force-needed-to-jump-towards-target-point.372288/"/>
	private void InitPhysicsJump()
	{
		Vector3 p = GameObject.FindGameObjectWithTag("Target").transform.position;
		float gravity = Physics.gravity.magnitude;

		// selected angle in radians
		float angle = initialAngle * Mathf.Deg2Rad;

		// position of this object and the target on the same plane
		Vector3 planarTarget = new Vector3(p.x, 0, p.z);
		Vector3 planarPosition = new Vector3(transform.position.x, 0, transform.position.z);

		// Planar distance between objects
		float distance = Vector3.Distance(planarTarget, planarPosition);

		// Distance along the y axis between objects
		float yoffset = transform.position.y - p.y;

		float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + yoffset));

		Vector3 velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));

		// Rotate our velocity to match the direction between the two objects
		float angleBetweenObjects = Vector3.Angle(Vector3.forward, planarTarget - planarPosition);
		Vector3 finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;


		rBody.velocity = finalVelocity;
		//rBody.AddForce(finalVelocity * rBody.mass, ForceMode.Impulse);

	}



	// From the jump height and gravity we deduce the upwards speed for the character to reach at the apex.
	private float CalculateJumpVerticalSpeed()
	{
		return Mathf.Sqrt(2f * jumpHeight * Mathf.Abs(Physics.gravity.y));
	}

	// Check if the base of the capsule is colliding to track if it's grounded
	private void TrackGrounded(Collision collision)
	{
		var maxHeight = capsule.bounds.min.y + capsule.radius * .9f;
		foreach (var contact in collision.contacts)
		{
			if (contact.point.y < maxHeight)
			{
				if (isKinematic(collision))
				{
					// Get the ground velocity and we parent to it
					groundVelocity = collision.rigidbody.velocity;
					transform.parent = collision.transform;
				}
				else if (isStatic(collision))
				{
					// Just parent to it since it's static
					transform.parent = collision.transform;
				}
				else
				{
					// We are standing over a dinamic object,
					// set the groundVelocity to Zero to avoid jiggers and extreme accelerations
					groundVelocity = Vector3.zero;
				}

				// Esta en el suelo
				grounded = true;
			}
			break;
		}
	}


	private bool isKinematic(Collision collision)
	{
		return isKinematic(collision.transform);
	}

	private bool isKinematic(Transform t)
	{
		Rigidbody rb = t.GetComponent<Rigidbody>();
		return rb && rb.isKinematic;
	}

	private bool isStatic(Collision collision)
	{
		return isStatic(collision.transform);
	}

	private bool isStatic(Transform transform)
	{
		return transform.gameObject.isStatic;
	}
}
