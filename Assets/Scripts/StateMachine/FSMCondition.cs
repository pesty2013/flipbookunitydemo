﻿using UnityEngine;
using System.Collections;


namespace StateMachine
{
	/// <summary>
	/// This class checks specific conditions and determines whether a
	/// transition should occur.
	/// </summary>

	public delegate bool ConditionDelegate(params object[] objs);


	public class FSMCondition : FSMTransition
	{
		public FSMCondition(ConditionDelegate del)
		{
			condition = del;
		}
		public ConditionDelegate condition;


		/// <summary>
		/// Process the transition
		/// </summary>
		/// <param name="objs">parameters passed</param>
		/// <returns>
		/// boolean value as to whether the guard should be allowed or not.
		/// </returns>
		public bool Process(params object[] objs)
		{
			if (condition == null)
				return false;

			return condition(objs);
		}
	}
}
