﻿using UnityEngine;
using System.Collections.Generic;

namespace StateMachine
{

	/// <summary>
	/// The StateMachine encapsulates all the State related data and methods
	/// into a state machine class.  This way an agent can own an instance of
	/// a StateMachine and delegate the management of current states, global
	/// states, and previous states to it.
	/// 
	/// In a state machine each character occupies one state.  Normally, actions
	/// or behaviors are associated with each state.  So, as long as the character 
	/// remains in that state, it will continue carrying out the same action.
	/// 
	/// States are connected together by transitions.  Each transition leads from
	/// one state to another, the target state, and each has a set of associated
	/// conditions.  If the game determines that the conditions of a transition are 
	/// met, then the character changes state to the transition's target state.
	/// When a transition's conditions are met, it is said to trigger, and when the
	/// transition is followed to a new state, it has fired.
	/// </summary>
	/// <seealso cref="Artificial Intelligence for games 2nd Edition Chapter 5: Decision Making"/>
	/// <seealso cref="Programming Game AI by Example by Matt Buckland"/>
	public class FSMSystem
	{
		private Dictionary<StateID, FSMState> states;

		private StateID currentStateID;
		public StateID CurrentStateID { get { return currentStateID; } }


		private FSMState currentState;
		public FSMState CurrentState { get { return currentState; } }

		public GameObject Owner { get; set; }

		#region Constructors
		/// <summary>
		/// Default constructor
		/// </summary>
		public FSMSystem()
		{
			states = new Dictionary<StateID, FSMState>();
		}


		/// <summary>
		/// Constructor that initializes the new state system, and
		/// sets up the current gameobject owner
		/// </summary>
		/// <param name="owner"></param>
		public FSMSystem(GameObject owner)
		{
			Owner = owner;
			states = new Dictionary<StateID, FSMState>();
		}
		#endregion

		#region State Container Methods
		/// <summary>
		/// Add a new state to the curren system
		/// </summary>
		/// <param name="s">new FSMState to add to system</param>
		public void AddState(FSMState s)
		{
			if (s == null)
				Debug.LogError("Null FSMState reference not allowed");

			if (states.ContainsKey(s.ID))
			{
				Debug.LogError("ERROR: FSMState ID " + s.ID.ToString() + " already exists");
				return;
			}

			states.Add(s.ID, s);

			// if this is the first state, make it the initial state to work with,
			// I am not sure this is a good assumption to make.
			if (states.Count == 1)
			{
				currentState = s;
				currentStateID = s.ID;
				currentState.Enter();
				return;
			}
		}


		/// <summary>
		/// Delete a state out of the current FSM system
		/// </summary>
		/// <param name="id"></param>
		public void DelState(StateID id)
		{
			if (states.ContainsKey(id))
			{
				states.Remove(id);
			}
			else
			{
				Debug.LogError("ERROR: FSMState ID " + id.ToString() + " doesn't exist");
			}
		}
		#endregion

		#region State Flow Methods
		/// <summary>
		/// ChangeState will force a transition to the new state
		/// passed as a parameter.  if the state doesn't exist or
		/// it is a null defined state, a error will be logged and
		/// nothing will change regarding the current state.
		/// 
		/// otherwise, the current state will exit() as normal
		/// and the state passed will become the current state.
		/// </summary>
		/// <param name="id">the new state ID</param>
		public void ChangeState(StateID id)
		{
			if (id != StateID.Null && id != currentState.ID)
			{
				if (states.ContainsKey(id))
				{
					if (currentState != null)
						currentState.Exit();

					currentStateID = id;
					currentState = states[id];
					currentState.Enter();
				}
				else
				{
					Debug.LogError("FSMSystem ERROR: " + id.ToString() + " state doesn't exist");
				}
			}
		}


		/// <summary>
		/// Perform transition trigger on a specific transition id being
		/// satified.  if the transition value is valid and contained 
		/// within the state transition tree, change state will trigger
		/// the transition to destination state.
		/// </summary>
		/// <param name="trans">transition</param>
		public void PerformTransition(Transition trans)
		{
			if (trans == Transition.Null)
			{
				Debug.LogError("Transition Error cannot be null");
				return;
			}

			StateID id = currentState.GetTransitionState(trans);
			ChangeState(id);
		}


		/// <summary>
		/// parse all of the conditional boolean expressions and
		/// change states if a specific condition for current state
		/// is satified.  Change State will trigger the transition
		/// if a real state ID is setup.
		/// </summary>
		public void PerformConditions()
		{
			StateID id = currentState.ProcessConditions(Owner);
			ChangeState(id);
		}
		#endregion
	}
}