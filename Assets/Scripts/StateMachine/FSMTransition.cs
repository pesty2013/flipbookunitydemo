﻿using UnityEngine;
using System.Collections;

namespace StateMachine
{
	public class FSMTransition
	{
		public Transition TransID { get; set; }
		public StateID StateID { get; set; }
	}
}
