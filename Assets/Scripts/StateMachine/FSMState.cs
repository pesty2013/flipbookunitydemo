﻿using UnityEngine;
using System.Collections.Generic;


namespace StateMachine
{
	/// <summary>
	///	State Design Pattern 
	/// 
	/// Each of a game agent's states is implemented as a unique class and 
	/// each agent holds a pointer to an instance of its current state.  An 
	/// agent also implements a ChangeState member function that can be 
	/// called to facilitate the switching of states whenever a state transition 
	/// is required.  The logic for determining any state transitions is 
	/// contained within each State class.  All state classes are derived 
	/// from an abstract base class, thereby defining a common interface.
	/// 
	/// It is favourable for each states to have associated enter and exit 
	/// actions.  This permits the programmer to write logic that is only 
	/// executed once at state entry and exit and increases the flexibility 
	/// of an FSM a great deal.
	/// </summary>
	public abstract class FSMState
	{
		protected Dictionary<Transition, StateID> transitions = new Dictionary<Transition, StateID>();
		protected Dictionary<ConditionDelegate, StateID> conditions = new Dictionary<ConditionDelegate, StateID>();

		protected StateID stateID;
		public StateID ID { get { return stateID; } }

		public GameObject Owner { get; set; }



		/// <summary>
		/// Add a specific condition that determines whether or not we should
		/// transition to another state.
		/// </summary>
		/// <param name="cond"></param>
		/// <param name="id"></param>
		public void AddCondition(ConditionDelegate cond, StateID id)
		{
			conditions.Add(cond, id);
		}


		/// <summary>
		/// Add a transition to the current state
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="id"></param>
		public void AddTransition(Transition trans, StateID id)
		{
			if (trans == Transition.Null)
			{
				Debug.LogError("FSMState ERROR: Null Transitions is not allowed");
				return;
			}


			if (id == StateID.Null)
			{
				Debug.LogError("FSMState ERROR: Null StateID is not allowed");
				return;
			}

			if (transitions.ContainsKey(trans))
			{
				Debug.LogError("FSMState ERROR: State " + trans.ToString() + " transitions already assigned to " + stateID.ToString());
				return;
			}
			transitions.Add(trans, id);
		}



		/// <summary>
		/// Delete a transition from the current state
		/// </summary>
		/// <param name="trans"></param>
		/// <param name="id"></param>
		public void DelTransition(Transition trans, StateID id)
		{
			if (transitions.ContainsKey(trans))
			{
				transitions.Remove(trans);
				return;
			}
			Debug.LogError("FSMState ERROR: Transition " + trans.ToString() + " isn't on the transitions collection");
		}


		/// <summary>
		/// Called once, when the state is initialized.
		/// </summary>
		/// <param name="Owner">who owns this</param>
		public virtual void Enter()
		{

		}



		/// <summary>
		/// Called every State update
		/// </summary>
		/// <param name="Owner">who</param>
		public virtual void Execute()
		{

		}



		/// <summary>
		/// Called once the state exits or state changes.
		/// </summary>
		/// <param name="Owner">who</param>
		public virtual void Exit()
		{

		}



		/// <summary>
		/// Method returns the new state id the FSM should be if
		/// the transition is valid, otherwise it will return a
		/// null state ID.
		/// </summary>
		/// <param name="trans">transition value</param>
		/// <returns></returns>
		public StateID GetTransitionState(Transition trans)
		{
			if (transitions.ContainsKey(trans))
			{
				return transitions[trans];
			}
			return StateID.Null;
		}


		/// <summary>
		/// Process the conditions for a state transition
		/// </summary>
		/// <param name="go"></param>
		/// <returns></returns>
		public StateID ProcessConditions(GameObject go)
		{
			object[] options = new object[1];
			options[0] = Owner;

			foreach (ConditionDelegate cond in conditions.Keys)
			{
				if (cond(options) == true)
				{
					return conditions[cond];
				}
			}
			return StateID.Null;
		}
	}
}