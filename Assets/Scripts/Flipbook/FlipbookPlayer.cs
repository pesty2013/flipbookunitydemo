﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using Flipbook;
using System;

public class FlipbookPlayer : MonoBehaviour 
{
	#region Message Handlers
	public delegate bool FlipbookMessageHandler(string msg, object[] options);
	public static event FlipbookMessageHandler flipbookTrigger;
	#endregion

	#region Unity Components
	protected GameObject spriteLayer;
	protected SpriteRenderer spriteRenderer;
	protected CelLayer celLayer;
	#endregion

	#region Playback variables
	private ClipData curClip;				// current clipdata being played
	private string curClipName;				// current clip name
	private float elapsedTime = 0;			// elapsed time during playback
	private float playbackSpeed = 1.0f;		// current playback speed
	private int frame = 0;					// current frame
	private bool bTriggered = false;        // any events triggered
	private bool bPaused = false;
	private bool clipDone = false;
	#endregion

	#region Properties
	private Texture2D spriteMapFile;
	public Texture2D SpriteAtlasImage
	{
		get { return spriteMapFile; }
		set { spriteMapFile = value; }
	}


	/// <summary>
	/// dictionary of sprites for all of the assets
	/// contained in this character
	/// </summary>
	Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
	public Dictionary<string, Sprite> SpriteContainer
	{
		get { return sprites; }
		set { sprites = value; }
	}


	/// <summary>
	/// clips for contained in the flipbook assets
	/// </summary>
	Clips clipContainer;
	public Clips ClipContainer
	{
		get { return clipContainer; }
		set { clipContainer = value; }
	}

	/// <summary>
	/// collection of all of the clips that are available 
	/// to be called using these assets
	/// </summary>
	Dictionary<string, ClipData> clips = new Dictionary<string, ClipData>();
	public Dictionary<string, ClipData> Clips
	{
		get { return clips; }
		set { clips = value; }
	}
	#endregion

	#region Collisions
	public List<ClipBoundingData> BBoxes { get; protected set; }
	public List<ClipBSphereData> BSpheres { get; protected set; }
	public List<ClipBLineData> BLines { get; protected set; }
	#endregion

	/// <summary>
	/// play the requested animation on the screen
	/// </summary>
	/// <param name="name">name of the animation</param>
	public void Play(string name)
	{
		// check if we are already playing the current animation
		if (name != curClipName)
		{
			ClipData clipdata;

			clips.TryGetValue(name, out clipdata);
			if (clipdata != null)
			{
				bTriggered = false;
				frame = 0;
				curClipName = name;
				curClip = clipdata;
				playbackSpeed = 1f / (float)clipdata.Fps;
				clipDone = false;
			}
		}
	}


	/// <summary>
	/// initialize the component with the cellayer, spritelayer
	/// and the spriterenderer
	/// 
	/// initialize the cel component for the player.  store the
	/// sprite layer into the parent, so we know where to put
	/// the sprite animations.
	/// </summary>
	public void InitPlayer()
	{
		Transform tran = transform.FindChild("Cel");
		if (tran)
		{
			spriteLayer = tran.gameObject;
			celLayer = spriteLayer.GetComponent<CelLayer>();
			if (celLayer == null)
			{
				celLayer = spriteLayer.AddComponent<Flipbook.CelLayer>();
			}
			spriteRenderer = tran.GetComponent<SpriteRenderer>();
		}
	}


	/// <summary>
	/// Pause the playback
	/// </summary>
	public void Pause()
	{
		bPaused = true;
	}


	/// <summary>
	/// unpause the playback
	/// </summary>
	public void UnPause()
	{
		bPaused = false;
	}

	/// <summary>
	/// play next frame 
	/// </summary>
	public void NextFrame()
	{
		elapsedTime = playbackSpeed + 1f;
	}



	/// <summary>
	/// Update the collider information about the frame
	/// </summary>
	/// <param name="frameData"></param>
	private void UpdateCollider(ClipFrameData frameData)
	{
		if (frameData != null)
		{
			if (frameData.BBoxes.Count != 0)
				BBoxes = frameData.BBoxes;
			else
				BBoxes = null;

			if (frameData.BSpheres.Count != 0)
				BSpheres = frameData.BSpheres;
			else
				BSpheres = null;

			if (frameData.BLines.Count != 0)
				BLines = frameData.BLines;
			else
				BLines = null;
		}
	}


	/// <summary>
	/// Trigger data represents a single trigger event that has been
	/// defined in a specific frame and is sent to the gameobject as
	/// an event.
	/// </summary>
	/// <param name="frameData"></param>
	private void UpdateTriggers(ClipFrameData frameData)
	{
		// check if the events have been triggered for this
		// frame or not.
		if (bTriggered == false)
		{
			// no .. not yet, so parse through the triggers and call
			// the tags.
			foreach (ClipTriggerData triggers in frameData.Triggers)
			{
				object[] objs = new object[2];
				objs[0] = triggers.Tag;
				objs[1] = GetInstanceID();

				OnTrigger(triggers.Tag, objs);
			}

			// set that the events have been triggered for this frame
			bTriggered = true;
		}
	}


	/// <summary>
	/// Toggle which way we are facing
	/// </summary>
	public void OnFlip()
	{
#if false
		Vector3 theScale = spriteLayer.transform.localScale;
		theScale.x *= -1;
		spriteLayer.transform.localScale = theScale;
#else
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;

#endif
	}

	#region Unity
	/// <summary>
	/// update the animation
	/// </summary>
	void Update () 
	{
		if (bPaused == false)
			elapsedTime += Time.deltaTime;

		if (curClip != null)
		{
			if (elapsedTime > playbackSpeed)
			{
				elapsedTime -= playbackSpeed;
                bTriggered = false;
				frame++;
			}

			if (frame >= curClip.NumFrames)
			{
				if (curClip.Playback == ePlayback.PlaybackLoop)
				{
					frame = 0;
				}
				else
				{
					frame = curClip.NumFrames - 1;

					// determine if we have sent clipDone event
					if (clipDone == false)
					{
						string msg = "onClipDone" + curClipName;
						object[] options = new object[1];
						options[0] = GetInstanceID();

						OnTrigger(msg, options);
						clipDone = true;
					}
				}
			}

			var frameData = curClip.Frames[frame];

			
			UpdateTriggers(frameData);
			UpdateCollider(frameData);


			var spriteData = frameData.Sprites[0];
			Sprite sprite;
			sprites.TryGetValue(spriteData.Name, out sprite);

			if (spriteRenderer != null && sprite != null)
			{
				Vector3 offset = new Vector3(spriteData.XOffset, -spriteData.YOffset, 0);
				Vector3 scale = spriteLayer.transform.localScale;
				offset.x *= scale.x;

				spriteLayer.transform.localPosition = offset / 100f;
				celLayer.currentSprite = sprite;
				celLayer.currentName = spriteData.Name;
                spriteRenderer.sprite = sprite;
			}
		}
	}


    void LateUpdate()
    {
//        Camera mainCamera = Camera.main;
//        transform.LookAt(mainCamera.transform.position, Vector3.up);
    }

#endregion

#region Flipbook Message Dispatcher
	public void Subscribe(GameObject go, FlipbookMessageHandler handler)
	{
		flipbookTrigger += handler;
	}


	public void UnSubscribe(GameObject go, FlipbookMessageHandler handler)
	{
		flipbookTrigger -= handler;
	}


	protected virtual bool OnTrigger(string msg, object[] options)
	{
		if (flipbookTrigger != null)
		{
			// dispatch the message
			return flipbookTrigger(msg, options);
		}
		return false;
	}
#endregion
}
