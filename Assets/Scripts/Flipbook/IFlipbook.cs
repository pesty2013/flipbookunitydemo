﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flipbook
{
	interface IFlipbook
	{
		FlipbookPlayer flipbook { get; set; }
		bool OnFlipbookMessage(string msg, object[] options);
		
	}
}
