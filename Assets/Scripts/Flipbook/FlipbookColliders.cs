﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flipbook;
using UnityEngine;



public class FlipbookColliders
{

	ClipBSphereData sphereA;
	ClipBSphereData sphereB;
	ClipBLineData lineA;
	ClipBLineData lineB;
	ClipBoundingData bboxA;
	ClipBoundingData bboxB;


	public FlipbookColliders()
	{
		sphereA.Radius = 10;
		sphereA.XOffset = 0;
		sphereB.YOffset = 0;

		sphereB.Radius = 5;
		sphereB.XOffset = 0;
		sphereB.YOffset = 0;

		Vector3 collide = new Vector3();

		bool ret = Sphere2Sphere(sphereA, sphereB, out collide);
		Debug.Log("Test 1: " + ret.ToString());


		sphereB.XOffset = 100;
		sphereB.YOffset = 100;


		ret = Sphere2Sphere(sphereA, sphereB, out collide);
		Debug.Log("Test 2: " + ret.ToString());

		Debug.Log("AllDone!");
	}



	/// <summary>
	/// do these bounding boxes overlap
	/// </summary>
	/// <param name="a"></param>
	/// <param name="b"></param>
	/// <returns></returns>
	public bool BBox2BBox(ClipBoundingData a, ClipBoundingData b, out Vector3 collision)
	{
		collision = Vector3.zero;

		return false;
	}
	


	/// <summary>
	/// do these spheres overlap.  simple overlap function that works on the 
	/// principle of calculating the distance between the center points and 
	/// then checking if the radius is within the radius.
	/// </summary>
	/// <param name="a">bounding sphere a</param>
	/// <param name="b">bounding sphere b</param>
	/// <returns></returns>
	public bool Sphere2Sphere(ClipBSphereData a, ClipBSphereData b, out Vector3 collision)
	{
		Vector2 pA = new Vector2(a.XOffset, a.YOffset);
		Vector2 pB = new Vector2(b.XOffset, b.YOffset);

		float dist = (pA - pB).magnitude;

		float minDist = a.Radius + b.Radius;

		collision = Vector3.zero;

		return (dist <= minDist);
	}




	/// <summary>
	/// do these lines cross each other
	/// </summary>
	/// <param name="a"></param>
	/// <param name="b"></param>
	/// <returns></returns>
	public bool Line2Line(ClipBLineData a, ClipBLineData b, out Vector3 collision)
	{

		collision = Vector3.zero;


		return false;
	}


	public bool Line2Sphere(ClipBSphereData sphere, ClipBLineData line, out Vector3 collision)
	{
		Vector3 seg_a = new Vector3(line.XStart, 0, line.YStart);
		Vector3 seg_b = new Vector3(line.XEnd, 0, line.YEnd);

		float circ_rad = sphere.Radius;
		Vector3 circ_pos = new Vector3(sphere.XOffset, 0, sphere.YOffset);

		Vector3 seg_v = seg_b - seg_a;				// calculate the line vector
		Vector3 pt_v = circ_pos - seg_a;            // calcuate vector from circle position to start line segment.

		// closest point to the circle enter on the segment
		// find the closest point to the circle's center on the line segment.  
		// To project on vector onto another, take the dot product of the vector and the unit vector of the projection target

		Vector3 seg_v_unit = seg_v / seg_v.magnitude;
		float proj = Vector3.Dot(pt_v, seg_v_unit);

		if (proj <= 0)
		{
			collision = seg_a;
			return true;
		}

		if (proj >= seg_v.magnitude)
		{
			collision = seg_b;
			return true;
		}

		Vector3 proj_v = seg_v_unit * proj;
		Vector3 closest = proj_v + seg_a;

		collision = closest;
		return true;
	}




	public bool TestSuccess()
	{
		ClipBSphereData sphere = new ClipBSphereData();
		ClipBLineData line = new ClipBLineData();
		Vector3 collision;


		sphere.Radius = 1;
		sphere.XOffset = 0;
		sphere.YOffset = 10;

		line.XStart = 10;
		line.YStart = 0;
		line.XEnd = -10;
		line.YEnd = 0;

		bool bRet = Line2Sphere(sphere, line, out collision);

		return false;
	}
}
