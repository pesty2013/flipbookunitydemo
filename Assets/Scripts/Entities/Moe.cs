﻿using UnityEngine;
using System.Collections;
using StateMachine;

public class Moe : BaseEntity
{
	#region Fields
	private FSMSystem stateMachine;
	private FlipbookPlayer flipbookPlayer;
	private BaseController simpleMove;
	private Vector3 moveDir;
	private bool jumping;
	private Vector3 spawnPos;
	#endregion

	#region Properties
	public FSMSystem FSM
	{
		get { return stateMachine; }
		set { stateMachine = value; }
	}
	#endregion

	#region Unity Methods
	// Use this for initialization
	public override void Start()
	{
		flipbookPlayer = GetComponent<FlipbookPlayer>();
		simpleMove = GetComponent<BaseController>();

		FSM = new FSMSystem(this.gameObject);
		RegisterTransitions();
		FSM.ChangeState(StateID.Idle);

		// subscribe to the flipbook messages ... 
		flipbookPlayer.Subscribe(gameObject, OnFlipbookMessage);

		spawnPos = transform.position;
		base.Start();
	}


	
	public override void Update()
	{
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		moveDir = new Vector3(h, 0, v);				// save new direction
		simpleMove.MoveDirection = moveDir;         // setup new direction

		jumping = Input.GetButton("Jump");
	}


	/// <summary>
	/// Update the 
	/// </summary>
	public void FixedUpdate()
	{
		FSM.PerformConditions();                    // perform state machine conditions
		FSM.CurrentState.Execute();					// Execute the current state
	}



	void OnEnable()
	{
		flipbookPlayer = GetComponent<FlipbookPlayer>();
		if (flipbookPlayer != null)
		{
			flipbookPlayer.Subscribe(gameObject, OnFlipbookMessage);
		}
	}



	void OnDisable()
	{
		flipbookPlayer = GetComponent<FlipbookPlayer>();
		if (flipbookPlayer != null)
		{
			flipbookPlayer.UnSubscribe(gameObject, OnFlipbookMessage);
		}
	}
	#endregion

	#region Conditions
	public bool IsWalking(object[] objs)
	{
		GameObject go = (GameObject)objs[0];			// get the object
		Rigidbody rbody = go.GetComponent<Rigidbody>();	// get the physics component

		Moe moe = go.GetComponent<Moe>();
		float velocity = moe.moveDir.magnitude;			// grab the current velocity

		return (velocity > 0.0f && velocity < 0.5f) ? true : false;
	}

	public bool IsRunning(object[] objs)
	{
		GameObject go = (GameObject)objs[0];
		Rigidbody rbody = go.GetComponent<Rigidbody>();

		Moe moe = go.GetComponent<Moe>();
		float velocity = moe.moveDir.magnitude;

		return (velocity < 0.9f && velocity > 0.5f) ? true : false;
	}

	public bool IsIdle(object[] objs)
	{
		GameObject go = (GameObject)objs[0];
		Rigidbody rbody = go.GetComponent<Rigidbody>();
		float velocity = rbody.velocity.magnitude;

		return (velocity <= 0.05f) ? true : false;
	}


	public bool IsSprinting(object[] objs)
	{
		GameObject go = (GameObject)objs[0];
		Rigidbody rbody = go.GetComponent<Rigidbody>();
		Moe moe = go.GetComponent<Moe>();

		float velocity = moe.moveDir.magnitude;

		return (velocity > 0.9f) ? true : false;
	}


	public bool IsOnGround(object[] objs)
	{
		bool bRet = false;

		GameObject go = (GameObject)objs[0];
		if (go != null)
		{
			CharacterController ctrl = go.GetComponent<CharacterController>();
			if (ctrl != null)
			{
				bRet = ctrl.isGrounded;
			}
		}
		return bRet;
	}

	public bool IsGrounded(object[] objs)
	{
		GameObject go = (GameObject)objs[0];
		BaseController ctrl = go.GetComponent<BaseController>();

		return ctrl.Grounded();
	}


	public bool IsJumping(object[] objs)
	{
		return jumping;
	}
	#endregion

	#region Messages
	/// <summary>
	/// Message to reset the character
	/// </summary>
	public void OnResetMoe()
	{
		transform.position = spawnPos;
		FSM.ChangeState(StateID.Idle);
	}
	#endregion

	/// <summary>
	/// Create the state machine and it's conditions and transitions
	/// </summary>
	public void RegisterTransitions()
	{
		IdleState idleState = new IdleState(this.gameObject);
		idleState.AddCondition(new ConditionDelegate(IsJumping), StateID.Jump);
		idleState.AddCondition(new ConditionDelegate(IsWalking), StateID.Walk);
		idleState.AddCondition(new ConditionDelegate(IsRunning), StateID.Run);
		idleState.AddCondition(new ConditionDelegate(IsSprinting), StateID.Sprint);
		FSM.AddState(idleState);

		WalkState walkState = new WalkState(this.gameObject);
		walkState.AddCondition(new ConditionDelegate(IsJumping), StateID.Jump);
		walkState.AddCondition(new ConditionDelegate(IsIdle), StateID.Idle);
		walkState.AddCondition(new ConditionDelegate(IsRunning), StateID.Run);
		walkState.AddCondition(new ConditionDelegate(IsSprinting), StateID.Sprint);
		FSM.AddState(walkState);

		RunState runState = new RunState(this.gameObject);
		runState.AddCondition(new ConditionDelegate(IsJumping), StateID.Jump);
		runState.AddCondition(new ConditionDelegate(IsIdle), StateID.Idle);
		runState.AddCondition(new ConditionDelegate(IsWalking), StateID.Walk);
		runState.AddCondition(new ConditionDelegate(IsSprinting), StateID.Sprint);
		FSM.AddState(runState);

		SprintState sprintState = new SprintState(this.gameObject);
		sprintState.AddCondition(new ConditionDelegate(IsJumping), StateID.Jump);
		sprintState.AddCondition(new ConditionDelegate(IsIdle), StateID.Idle);
		sprintState.AddCondition(new ConditionDelegate(IsWalking), StateID.Walk);
		sprintState.AddCondition(new ConditionDelegate(IsRunning), StateID.Run);
		FSM.AddState(sprintState);

		JumpPhysicsState jumpState = new JumpPhysicsState(this.gameObject);
		jumpState.AddCondition(new ConditionDelegate(IsGrounded), StateID.Idle);
		FSM.AddState(jumpState);
	}


	/// <summary>
	/// The event handler for flipbook events.
	/// </summary>
	/// <param name="msg">what is the message</param>
	/// <param name="options">options associated with the message</param>
	/// <returns></returns>
	bool OnFlipbookMessage(string msg, object[] options)
	{
		bool bRet = false;

		Debug.Log(msg + ": was triggered");
//		bRet = FSM.HandleMessage(msg);

		if (bRet == false)
		{
//			Debug.Log("FSM: failed to handle message: " + msg);
		}
		return bRet;
	}
}
