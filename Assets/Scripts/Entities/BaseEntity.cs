﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[Serializable()]
public class BaseEntity : MonoBehaviour
{
	#region Properties

	[XmlElement("Position")]
	public Vector3 Position	{ get; set; }
	[XmlElement("Name")]
	public string Name	{ get; set; }

	[XmlElement("Mass")]
	public int Mass { get; set; }

	[XmlElement("Tag")]
	public string Tag { get; set; }

	[XmlElement("Health")]
	public int Health { get; set; }

	[XmlElement("Mind")]
	public Blackboard Mind { get; set; }        // The reference to the player's blackboard

	/// <summary>
	/// The debugging color associated with this entity
	/// </summary>
	[XmlElement("DebugColor")]
	public Color DebugColor
	{
		get { return DebugDrawColor; }
		set { DebugDrawColor = value; }
	}

	/// <summary>
	/// a bool that determines if a message is logged for this entity
	/// </summary>
	[XmlElement("DebugMessage")]
	public bool DebugMessage
	{
		get { return DebugMsg; }
		set { DebugMsg = value; }
	}
	#endregion


	#region Debugging
	public bool DebugMsg = false;
	public Color DebugDrawColor = Color.white;
	#endregion

	public delegate void BuildDelegate(Dictionary<string, string> attributes);

	#region Constructors
	public BaseEntity()
	{
		Init ();
	}
	#endregion

	#region Protected methods
	protected virtual void Init()
	{
	}
	#endregion

	#region Builder Methods
	/// <summary>
	/// method that takes the attributes passed during creating
	/// this instance and sets up all of the properties.
	/// </summary>
	/// <param name="attributes">dictionary class of attributes</param>
	public virtual void Builder(Dictionary<string, string> attributes)
	{
	}



	/// <summary>
	/// Create this entity and return it to caller
	/// </summary>
	/// <returns></returns>
	static public BaseEntity Create()
	{
		var entity = new BaseEntity ();
		return entity;
	}
	#endregion

	#region Unity
	public virtual void Start()
	{
		if (Position != null)
			transform.position = Position;

		if (Tag != null && Tag != "")
		{
			transform.tag = Tag;
		}

		if (Name != null)
			transform.name = Name;


		// Make the player face the correct way, otherwise the movement
		// is all messed up.  Also this allows the removal of fixedUpdate
		// from the facing the camera.
		Vector3 theScale = transform.localScale;
		theScale.x = -1;
		transform.localScale = theScale;
	}

	public virtual void Update()
	{
	}


	#endregion
}
