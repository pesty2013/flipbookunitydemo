﻿using UnityEngine;




/// <summary>
/// The basic structure of a blackboard system has three parts: a set of 
/// different decision making tools (experts), a blackboard, and an 
/// arbiter.  
/// 
/// The blackboard is an area of memory that any expert may use to read from
/// and write to.  Each expert needs to read and write in roughly the same
/// language, although there will usually be messages on the blackboard that 
/// not everyone can understand.
/// 
/// Each expert looks at the blackboard and decides if there's anything on it
/// that they can use.  If there is, they ask to be allowed to have the chalk
/// and board eraser for a while.  When they get control they can do some
/// thinking, remove information from the blackboard, and write new
/// information, as they see fit.  After a short time, the expert will 
/// relinquish control and allow other experts to have a go.
/// 
/// The arbiter picks which expert gets control at each go.  Experts need to have
/// some mechanism of indicating that they have something interesting to say.  The
/// arbiter chooses one at a time and gives it control.  
/// 
/// The algorithum works in iterations:
///		Experts look at the board and indicate interest.
///		The arbiter selects an expert to have control.
///		The expert does some work, possibly modifying the blackboard.
///		The expert voluntarily relinquishes control.
///		
/// The algorithum used by the arbiter can vary from implementation to 
/// implementation.
/// </summary>
public class Blackboard 
{
	public GameObject Target { get; set; }		// target game object (unity specific object)
	public Vector3 Origin { get; set; }         // starting location


	#region Constructors
	public Blackboard()
	{
		Target = null;
		Origin = Vector3.zero;
	}
	#endregion
}
