﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flipbook;
using UnityEngine;

class FlipbookEntity : BaseEntity, IFlipbook
{
	private FlipbookPlayer flipbookPlayer;
	public FlipbookPlayer flipbook
	{
		get
		{
			return flipbookPlayer;
		}

		set
		{
			flipbookPlayer = value;
		}
	}




	public override void Start()
	{
		flipbookPlayer = GetComponent<FlipbookPlayer>();

		if (flipbookPlayer != null)
		{
			// subscribe to the flipbook messages ...
			flipbookPlayer.Subscribe(gameObject, OnFlipbookMessage);
			flipbookPlayer.Play("Idle");
		}

		base.Start();
	}



	public bool OnFlipbookMessage(string msg, object[] options)
	{
		Debug.Log("[OnFlipbookMessage] " + msg + ": was triggered");
		return false;
	}


	private void OnEnable()
	{
		flipbookPlayer = GetComponent<FlipbookPlayer>();
		if (flipbookPlayer != null)
		{
			flipbookPlayer.Subscribe(gameObject, OnFlipbookMessage);
		}
	}

	private void OnDisable()
	{
		flipbookPlayer = GetComponent<FlipbookPlayer>();
		if (flipbookPlayer != null)
		{
			flipbookPlayer.UnSubscribe(gameObject, OnFlipbookMessage);
		}
	}
}


