﻿using StateMachine;

public enum StateID
{
	Null = 0,           // use this ID to represent a non-existing state in your system
	Idle,
	Walk,
	Run,
	Sprint,
	Jump
}


public enum Transition
{
	Null = 0,           // use this transition to represent a non-existing transition
						// within your system
	Stopped,
	SlowSpeed,
	WalkSpeed,
	RunSpeed,
	SprintSpeed,
	Jump
}

