﻿using UnityEngine;
using StateMachine;

public class WalkState : FSMState
{
	FlipbookPlayer flipbookPlayer;
	BaseEntity baseEntity;
	BaseController simpleMove;

	#region Constructors
	public WalkState(GameObject go)
	{
		Owner = go;
		stateID = StateID.Walk;

		// gather the relevant components for movement
		flipbookPlayer = Owner.GetComponent<FlipbookPlayer>();
		baseEntity = Owner.GetComponent<BaseEntity>();
		simpleMove = Owner.GetComponent<BaseController>();
	}
	#endregion

	#region State Methods
	/// <summary>
	/// locate all of the components in the character, and setup
	/// the walk modifier and play the walking animation
	/// </summary>
	public override void Enter()
	{
		simpleMove.MoveForce = 5f;

		// start the idle animation
		flipbookPlayer.Play("WalkCycle");

#if DEBUG
		if (baseEntity.DebugMsg)
		{
			Debug.Log("[" + baseEntity.Name + "] entering [" + this.ToString() + "] state");
		}
#endif
	}


	public override void Execute()
	{
		simpleMove.MoveUpdate();
	}
	#endregion
}

