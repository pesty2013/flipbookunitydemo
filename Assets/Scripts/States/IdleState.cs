﻿using UnityEngine;
using StateMachine;


/// <summary>
/// Idle state setup and play the idle animation
/// </summary>
public class IdleState : FSMState
{
	FlipbookPlayer flipbookPlayer;
	BaseController simpleMove;
	BaseEntity baseEntity;

	public IdleState(GameObject go)
	{
		Owner = go;
		stateID = StateID.Idle;
		flipbookPlayer = Owner.GetComponent<FlipbookPlayer>();
		simpleMove = Owner.GetComponent<BaseController>();
		baseEntity = Owner.GetComponent<BaseEntity>();
	}



	public override void Enter()
	{
		// start the idle animation
		flipbookPlayer.Play("Idle");
		simpleMove.MoveForce = 0f;

#if DEBUG
		if (baseEntity.DebugMsg)
		{
			Debug.Log("[" + baseEntity.Name + "] entering [" + this.ToString() + "] state");
		}
#endif
	}

	public override void Execute()
	{
		simpleMove.MoveUpdate();
	}
}
