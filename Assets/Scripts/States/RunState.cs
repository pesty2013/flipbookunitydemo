﻿using UnityEngine;
using StateMachine;

public class RunState : FSMState
{
	FlipbookPlayer flipbookPlayer;
	BaseController simpleMove;
	BaseEntity baseEntity;

	public RunState(GameObject go)
	{
		Owner = go;
		stateID = StateID.Run;

		flipbookPlayer = Owner.GetComponent<FlipbookPlayer>();
		simpleMove = Owner.GetComponent<BaseController>();
		baseEntity = Owner.GetComponent<BaseEntity>();

	}

	public override void Enter()
	{
		simpleMove.MoveForce = 7f;

		// start the idle animation
		flipbookPlayer.Play("RunCycle");


#if DEBUG
		if (baseEntity.DebugMsg)
		{
			Debug.Log("[" + baseEntity.Name + "] entering [" + this.ToString() + "] state");
		}
#endif
	}


	public override void Execute()
	{
		simpleMove.MoveUpdate();
	}

}
