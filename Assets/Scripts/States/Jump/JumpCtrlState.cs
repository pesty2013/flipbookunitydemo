﻿using UnityEngine;
using StateMachine;

public class JumpCtrlState : FSMState {

	FlipbookPlayer flipbookPlayer;
	BaseEntity baseEntity;
	SimpleMove simpleMove;
	CharacterController ctrl;
	Rigidbody rBody;

	Vector3 startPos;                 // holds the starting point of the jump
	Vector3 endPos;                 // holds the destination of the jump

	Vector3 moveDir;

	Vector3 planarDir;
	float planarDistance;

	float deltaY;

	float jumpTime;					// the jump duration required
	float gravity;
	float maxYVelocity;
	float maxSpeed;

	#region Constructor
	/// <summary>
	/// constructor finds all of the components that are required for the
	/// jump to happen.
	/// </summary>
	/// <param name="go">game object that is going to jump</param>
	public JumpCtrlState(GameObject go)
	{
		Owner = go;
		stateID = StateID.Jump;
		flipbookPlayer = Owner.GetComponent<FlipbookPlayer>();
		baseEntity = Owner.GetComponent<BaseEntity>();
		simpleMove = Owner.GetComponent<SimpleMove>();
		ctrl = Owner.GetComponent<CharacterController>();
		rBody = go.GetComponent<Rigidbody>();

		gravity = Physics.gravity.y;
	}
	#endregion



	/// <summary>
	/// Calculate the distance to travel between 2 points
	/// on the planar level.
	/// </summary>
	private void CalculatePlanar()
	{
		planarDir = endPos - startPos;
		planarDir.y = 0;
		planarDistance = planarDir.magnitude;
	}


	/// <summary>
	/// calculate the y-delta for the jump as this represents the 
	/// height which we must jump.
	/// </summary>
	private void CalculateYDelta()
	{
		deltaY = moveDir.y;
	}

	/// <summary>
	/// work out the trajectory time
	/// </summary>
	/// <returns>time</returns>
	private float CalculateTime()
	{
		float time = 0f;

		float term = Mathf.Sqrt(2 * gravity * moveDir.y + maxYVelocity * maxYVelocity);
		time = (maxYVelocity - term) / gravity;

		return time;
	}



	/// <summary>
	/// work out the trajectory calculation
	/// </summary>
	private void InitJump()
	{
		startPos = Owner.transform.position;
		endPos = GameObject.FindGameObjectWithTag("Target").transform.position;
		moveDir = endPos - startPos;

		maxYVelocity = 10f;

		jumpTime = CalculateTime();
		CalculatePlanar();
		CalculateYDelta();

		Debug.Log("JumpTime: " + jumpTime);
	}


	/// <summary>
	/// work out the new position based upon elapsed time, gravity, and move
	/// velocity
	/// </summary>
	/// <param name="elapsedTime"></param>
	/// <returns></returns>
	private Vector3 CalculateVelocity(float elapsedTime)
	{
		Vector3 delta = new Vector3();

		delta.x = startPos.x + moveDir.x * elapsedTime;
		delta.y = startPos.y + moveDir.y * elapsedTime + (gravity * elapsedTime * elapsedTime * 0.5f);
		delta.z = startPos.z = moveDir.z * elapsedTime;

		return delta;
	}


	#region State Methods
	/// <summary>
	/// Enter called once to initialize the jump ...
	/// </summary>
	public override void Enter()
	{
		// start playing the jump animation ...
		flipbookPlayer.Play("JumpCycle");

		Vector3 dir = simpleMove.MoveDirection;
		dir.y = 50f;
		simpleMove.MoveDirection = dir;

		rBody.AddForce(new Vector3(0, 50f, 0));
//		ctrl.SimpleMove(new Vector3(0, 50f, 0));
	}


	/// <summary>
	/// Execute the jump state
	/// </summary>
	public override void Execute()
	{
		Vector3 dir = simpleMove.MoveDirection;
		dir.y = 50f;
		simpleMove.MoveDirection = dir;
	}
	#endregion
}
