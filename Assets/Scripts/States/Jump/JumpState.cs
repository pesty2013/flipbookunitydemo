﻿using UnityEngine;
using StateMachine;

public struct JumpPoint
{
	public Vector3 jumpLocation;               // the position of the jump point.
	public Vector3 landingPosition;            // the position of the landing point / pad

	
	// The change in position from jump to landing
	// this is claculate from the other values
	public Vector3 deltaPosition;				
}


/// <summary>
/// This is a controlled jump using a controller, rather than physics so this will
/// calculate on each Execute()
/// </summary>
public class JumpState : FSMState
{
	FlipbookPlayer flipbookPlayer;
	BaseEntity baseEntity;
	SimpleMove simpleMove;
	CharacterController ctrl;

	float jumpSpeed = 8.0f;

	Vector3 moveDirection = Vector3.zero;
	float vSpeed = 0f;

	Transform targetPos;
	float initialAngle = 30f;


	// CTRL jump variables
	public float jumpDuration = 15f;

	private bool jumping = false;
	private float jumpStartVelocityY;

	float jumpTimeElapsed = 0;
	float jumpProgress = 0;
	float velocityY;
	float height;

	Vector3 startPoint;					// holds the starting point of the jump
	Vector3 targetPoint;				// holds the destination of the jump
	Vector3 moveJumpDir;				

	bool canAchieve;                    // tracks whether the jump is achievable


	float maxSpeed = 20f;						// the maximum speed of the player
	float maxYVelocity = 10f;           // the maximum velocity the player may jump

	JumpPoint jumpPoint;

	Vector3 targetVelocity;

	#region Constructor
	/// <summary>
	/// constructor finds all of the components that are required for the
	/// jump to happen.
	/// </summary>
	/// <param name="go">game object that is going to jump</param>
	public JumpState(GameObject go)
	{
		Owner = go;
		stateID = StateID.Jump;
		flipbookPlayer = Owner.GetComponent<FlipbookPlayer>();
		baseEntity = Owner.GetComponent<BaseEntity>();
		simpleMove = Owner.GetComponent<SimpleMove>();
		ctrl = Owner.GetComponent<CharacterController>();
	}
	#endregion


	private void MoveToPoint(Vector3 pt)
	{
		Vector3 movDiff = pt - Owner.transform.position;
		Vector3 movDir = movDiff.normalized * 15f * Time.deltaTime;

		if (movDir.sqrMagnitude < movDiff.sqrMagnitude)
		{
			ctrl.Move(movDir);
		}
		else
		{
			ctrl.Move(movDiff);
		}
	}


	/// <summary>
	/// Calculate the initial velocity of a jump based off gravity
	/// and desired maximum height attained.
	/// </summary>
	/// <param name="jumpHeight"></param>
	/// <param name="gravity"></param>
	/// <returns></returns>
	private float CalculateJumpSpeed(float jumpHeight, float gravity)
	{
		return Mathf.Sqrt(2 * jumpHeight * gravity);

	}

	private float calculateTarget()
	{
		jumpPoint.landingPosition = targetPoint = targetPos.position;
		jumpPoint.jumpLocation = startPoint = Owner.transform.position;
		jumpPoint.deltaPosition = targetPoint - startPoint;

		moveJumpDir = targetPos.position - Owner.transform.position;

		float sqrtTerm = Mathf.Sqrt(2 * Physics.gravity.y * 2 * jumpPoint.deltaPosition.y +
										maxYVelocity * maxYVelocity);                               // TODO: Check for Typo on the website ....


		float time = (maxYVelocity - sqrtTerm) / Physics.gravity.y;

		if (checkJumpTime(time) == false)
		{
			time = (maxYVelocity + sqrtTerm) / Physics.gravity.y;
			checkJumpTime(time);
		}

		return time;
	}


	private bool checkJumpTime(float time)
	{
		canAchieve = false;
		float vx = jumpPoint.deltaPosition.x / time;
		float vz = jumpPoint.deltaPosition.z / time;

		float speedSq = vx * vx + vz * vz;

		if (speedSq < maxSpeed * maxSpeed)
		{
			targetVelocity = new Vector3(vx, 0, vz);
			canAchieve = true;
		}
		return canAchieve;
	}


	private void InitCtrlJump()
	{
		jumpDuration = calculateTarget();


		// given distance and jump duration, there is only one possible 
		// movement curve.  We are executing y-axis movement separately,
		// so we need to know a starting velocity for the y-axis
		jumpStartVelocityY = -jumpDuration * Physics.gravity.y / 2f;
		jumping = true;
		jumpProgress = 0;
		jumpTimeElapsed = 0;
		velocityY = jumpStartVelocityY;
		height = startPoint.y;
	}



	/// <summary>
	/// Jump in a certain direction
	/// </summary>
	/// <param name="direction"></param>
	/// <returns></returns>
	private bool Jump(Vector3 direction)
	{
		jumpProgress = jumpTimeElapsed / jumpDuration;

		if (jumpProgress > 1)
		{
			jumping = false;
			jumpProgress = 1;
		}

		// planar interpolation for jump
		Vector3 currentPos = Vector3.Lerp(startPoint, targetPoint, jumpProgress);
		currentPos.y = height;

		MoveToPoint(currentPos);

		// adjust the height ...
		height += (velocityY * Time.deltaTime);
		velocityY += (Time.deltaTime * Physics.gravity.y);

		// update the time elapsed
		jumpTimeElapsed += Time.deltaTime;

		if (jumping == false)
		{
			MoveToPoint(targetPoint);
		}
		return jumping;
	}


	/// <summary>
	/// Initialize the jump and calculate the 
	/// </summary>
	/// <returns></returns>
	private Vector3 InitJump()
	{
		Vector3 v = Vector3.zero;
		targetPos = GameObject.FindGameObjectWithTag("Target").transform;
		startPoint = Owner.transform.position;
		targetPoint = targetPos.position;

		v = new Vector3(targetPoint.x - startPoint.x, targetPoint.y - startPoint.y, targetPoint.z - startPoint.z);

		return v;
	}

	private Vector3 CalculateJump(Vector3 v)
	{
		Vector3 pos = Vector3.zero;

		jumpProgress = jumpTimeElapsed / jumpDuration;

		pos.x = startPoint.x + v.x * jumpProgress;
		pos.z = startPoint.z = v.z * jumpProgress;


		jumpTimeElapsed += Time.deltaTime;

		return pos;
	}

	/// <summary>
	/// Enter called once to initialize the jump ...
	/// </summary>
	public override void Enter()
	{
		// start playing the jump animation ...
		flipbookPlayer.Play("JumpCycle");

		if (ctrl.isGrounded)
		{
			vSpeed = jumpSpeed;
		}
		else
		{
			vSpeed = -1;
		}

		targetPos = GameObject.FindGameObjectWithTag("Target").transform;
		InitCtrlJump();
	}


	/// <summary>
	/// Execute the jump state
	/// </summary>
	public override void Execute()
	{
		bool bDone = Jump(moveJumpDir);
	}
}
