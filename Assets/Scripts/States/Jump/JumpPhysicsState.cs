﻿using UnityEngine;
using StateMachine;

public class JumpPhysicsState : FSMState
{
	FlipbookPlayer flipbookPlayer;
	BaseEntity baseEntity;
	Rigidbody rigid;


	Transform targetPos;
	float initialAngle = 30f;



	#region Constructor
	/// <summary>
	/// constructor finds all of the components that are required for the
	/// jump to happen.
	/// </summary>
	/// <param name="go">game object that is going to jump</param>
	public JumpPhysicsState(GameObject go)
	{
		Owner = go;
		stateID = StateID.Jump;
		flipbookPlayer = Owner.GetComponent<FlipbookPlayer>();
		baseEntity = Owner.GetComponent<BaseEntity>();
		rigid = Owner.GetComponent<Rigidbody>();
	}
	#endregion

	#region Physics Jump
	/// <summary>
	/// This sets up a physics jump, by solving object's motion along the 
	/// two axes separately.  Given values, position of target (target), our
	/// position (Owner), and the angle that we want to jump at.  From this, 
	/// we know the planar distance between the object (no y-axis) and the
	/// offset on the y-axis between the two.
	/// 
	/// </summary>
	/// <seealso cref="http://forum.unity3d.com/threads/how-to-calculate-force-needed-to-jump-towards-target-point.372288/"/>
	private void InitPhysicsJump()
	{
		Vector3 p = targetPos.position;
		float gravity = Physics.gravity.magnitude;

		// selected angle in radians
		float angle = initialAngle * Mathf.Deg2Rad;

		// position of this object and the target on the same plane
		Vector3 planarTarget = new Vector3(p.x, 0, p.z);
		Vector3 planarPosition = new Vector3(Owner.transform.position.x, 0, Owner.transform.position.z);

		// Planar distance between objects
		float distance = Vector3.Distance(planarTarget, planarPosition);

		// Distance along the y axis between objects
		float yoffset = Owner.transform.position.y - p.y;

		float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + yoffset));

		Vector3 velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));

		// Rotate our velocity to match the direction between the two objects
		float angleBetweenObjects = Vector3.Angle(Vector3.forward, planarTarget - planarPosition);
		Vector3 finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;

		// jump
		// rigid.velocity = finalVelocity;
		rigid.AddForce(finalVelocity * rigid.mass, ForceMode.Impulse);
	}
	#endregion


	/// <summary>
	/// Enter called once to initialize the jump ...
	/// </summary>
	public override void Enter()
	{
		// start playing the jump animation ...
		flipbookPlayer.Play("JumpCycle");

		targetPos = GameObject.FindGameObjectWithTag("Target").transform;
		InitPhysicsJump();
	}



	/// <summary>
	/// Execute the jump state
	/// </summary>
	public override void Execute()
	{
	}
}
