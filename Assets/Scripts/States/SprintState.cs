﻿using UnityEngine;
using StateMachine;

public class SprintState : FSMState
{
	FlipbookPlayer flipbookPlayer;
	BaseController simpleMove;
	BaseEntity baseEntity;

	public SprintState(GameObject go)
	{
		Owner = go;
		stateID = StateID.Sprint;

		// gather the relevant components that are needed
		flipbookPlayer = Owner.GetComponent<FlipbookPlayer>();
		simpleMove = Owner.GetComponent<BaseController>();
		baseEntity = Owner.GetComponent<BaseEntity>();
	}

	public override void Enter()
	{
		simpleMove.MoveForce = 10f;

		// start the idle animation
		flipbookPlayer.Play("FastRun");

		base.Enter();
#if DEBUG
		if (baseEntity.DebugMsg)
		{
			Debug.Log("[" + baseEntity.Name + "] entering [" + this.ToString() + "] state");
		}
#endif
	}


	public override void Execute()
	{
		simpleMove.MoveUpdate();

	}

}
