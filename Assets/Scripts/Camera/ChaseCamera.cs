﻿using UnityEngine;
using System.Collections;


public class BaseCamera : MonoBehaviour
{
	public Vector3 Up { get; set; }
	public float aspectRatio { get; set; }
	public float nearPlaneDistance { get; set; }
	public float farPlaneDistance { get; set; }
	
	public GameObject Target { get; set; }
	public Vector3 cameraVelocity { get; set; }

	public Camera baseCamera { get; set; }


	public void Awake()
	{
		Target = GameObject.FindGameObjectWithTag("Player");
	}

	public virtual void Start()
	{
		aspectRatio = GetComponent<Camera>().aspect;
		nearPlaneDistance = GetComponent<Camera>().nearClipPlane;
		farPlaneDistance = GetComponent<Camera>().farClipPlane;

	}


	public virtual void Update()
	{

	}

}

public class ChaseCamera : MonoBehaviour
{
	#region BaseCamera stuff
	public Vector3 Up { get; set; }
	public float aspectRatio { get; set; }
	public float nearPlaneDistance { get; set; }
	public float farPlaneDistance { get; set; }

	public GameObject Target = null;
	public Vector3 cameraVelocity { get; set; }

	public Camera baseCamera { get; set; }
	#endregion


	#region Chased object properties (set externally each frame)
	/// <summary>
	/// position of the object being chased
	/// 
	/// </summary>
	public Vector3 ChasePosition { get; set; }


	/// <summary>
	/// direction the chased object is facing
	/// </summary>
	public Vector3 ChaseDirection { get; set; }
	#endregion


	#region Desired camera positioning (set when creating camera or changing view)

	/// <summary>
	/// Desired camera position in the chased object's coordinate system.
	/// </summary>
	public Vector3 DesiredPositionOffset = new Vector3(0, 2.0f, 7.0f);

	/// <summary>
	/// Desired camera position in world space.
	/// </summary>
	public Vector3 DesiredPosition
	{
		get
		{
			// Ensure correct value even if update has not been called this frame
			UpdateWorldPositions();

			return desiredPosition;
		}
	}
	private Vector3 desiredPosition;

	/// <summary>
	/// Look at point in the chased object's coordinate system.
	/// </summary>
	public Vector3 LookAtOffset = new Vector3(0, 2.8f, 0);
	

	/// <summary>
	/// Look at point in world space.
	/// </summary>
	public Vector3 LookAt
	{
		get
		{
			// Ensure correct value even if update has not been called this frame
//			UpdateWorldPositions();

			return lookAt;
		}
	}
	private Vector3 lookAt;
	#endregion


	#region Camera physics (typically set when creating camera)

	/// <summary>
	/// Physics coefficient which controls the influence of the camera's position
	/// over the spring force. The stiffer the spring, the closer it will stay to
	/// the chased object.
	/// </summary>
	public float Stiffness = 4000f;


	/// <summary>
	/// Physics coefficient which approximates internal friction of the spring.
	/// Sufficient damping will prevent the spring from oscillating infinitely.
	/// </summary>
	public float Damping = 600f;


	/// <summary>
	/// Mass of the camera body. Heaver objects require stiffer springs with less
	/// damping to move at the same rate as lighter objects.
	/// </summary>
	public float Mass = 50f;

	#endregion

	public void Awake()
	{
		Target = GameObject.FindGameObjectWithTag("Player");
	}


	// Use this for initialization
	void Start () 
	{

//		base.Start();			

		aspectRatio = GetComponent<Camera>().aspect;
		nearPlaneDistance = GetComponent<Camera>().nearClipPlane;
		farPlaneDistance = GetComponent<Camera>().farClipPlane;
		baseCamera = Camera.main;

//		Stiffness = 4000.0f;
//		Damping = 600f;



//		DesiredPositionOffset = new Vector3(0.0f, 2.0f, 10.0f);
//		LookAtOffset = new Vector3(0.0f, 1.0f, 0.0f);

//		LookAtOffset = new Vector3(0, 2.8f, 0);
//		DesiredPositionOffset = new Vector3(0, 2.0f, 7.0f);
	
		ChaseDirection = Vector3.forward;
		Up = Vector3.up;


//		UpdateProjection();
	}
	

	#region Methods


	/// <summary>
	/// converted XNA TransformNormal over to Unity.
	/// </summary>
	/// <param name="normal"></param>
	/// <param name="matrix"></param>
	/// <returns></returns>
	Vector3 TransformNormal(Vector3 normal, Matrix4x4 matrix)
	{
		Vector3 transformNormal = new Vector3();
		Vector3 axisX = new Vector3(matrix.m00, matrix.m01, matrix.m02);
		Vector3 axisY = new Vector3(matrix.m10, matrix.m11, matrix.m12);
		Vector3 axisZ = new Vector3(matrix.m20, matrix.m21, matrix.m22);
		transformNormal.x = Vector3.Dot(normal, axisX);
		transformNormal.y = Vector3.Dot(normal, axisY);
		transformNormal.z = Vector3.Dot(normal, axisZ);
 
		return transformNormal;
	}




	/// <summary>
	/// Rebuilds object space values in world space. Invoke before publicly
	/// returning or privately accessing world space values.
	/// </summary>
	private void UpdateWorldPositions()
	{
		Target = GameObject.FindGameObjectWithTag("Player");


		// update the target position
		if (Target != null)
		{
			ChasePosition = Target.transform.position;

#if false
			int mult = targetEntity.Facing == PhysicsEntity.eFACING.eFaceRight ? 1 : -1;
			Vector3 range = new Vector3(mult, 0, 0);
			range = Vector3.Multiply(range, 1);
	
			chasePosition = Vector3.Add(range, chasePosition);
#endif
			ChaseDirection = Vector3.Normalize(baseCamera.transform.position - ChasePosition);

		}
		else
		{
			// no target, so look at the center of the world.
			ChasePosition = Vector3.zero;
			ChaseDirection = Vector3.Normalize(baseCamera.transform.position - ChasePosition);
		}

		// Construct a matrix to transform from object space to worldspace
		Matrix4x4 cameraMatrix = new Matrix4x4();

//		transform.Forward = ChaseDirection;
//		transform.Right = Vector3.Cross(Up, ChaseDirection);
#if true
		cameraMatrix.SetColumn(0, Vector3.Cross(Up, ChaseDirection));
		cameraMatrix.SetColumn(1, Vector3.up);
		cameraMatrix.SetColumn(2, -Vector3.forward);
#else
		CamMat.SetRow(0, new Vector4(-Target.forward.x, -Target.forward.y, -Target.forward.z));
		CamMat.SetRow(1, new Vector4(Target.up.x, Target.up.y, Target.up.z));
		Vector3 modRight = Vector3.Cross(CamMat.GetRow(1), CamMat.GetRow(0));
		CamMat.SetRow(2, new Vector4(modRight.x, modRight.y, modRight.z));
 

#endif

		// Calculate desired camera properties in world space
		desiredPosition = ChasePosition + TransformNormal(DesiredPositionOffset, cameraMatrix);
		lookAt = ChasePosition + TransformNormal(LookAtOffset, cameraMatrix);

		baseCamera.transform.LookAt(lookAt, Up);
	}

	/// <summary>
	/// Rebuilds camera's view and projection matricies.
	/// </summary>
	private void UpdateMatrices()
	{
//		viewMatrix = Matrix.CreateLookAt(this.Position, this.LookAt, this.Up);
//		projectionMatrix = Matrix.CreatePerspectiveFieldOfView(FieldOfView,
//			AspectRatio, NearPlaneDistance, FarPlaneDistance);
	}

	/// <summary>
	/// Forces camera to be at desired position and to stop moving. The is useful
	/// when the chased object is first created or after it has been teleported.
	/// Failing to call this after a large change to the chased object's position
	/// will result in the camera quickly flying across the world.
	/// </summary>
	public void Reset()
	{
		UpdateWorldPositions();

		// Stop motion
		cameraVelocity = Vector3.zero;

		// Force desired position
		gameObject.transform.position = desiredPosition;
//			position = new Vector3(0f, 5.0f, 10.0f);

		UpdateMatrices();
	}


	/// <summary>
	/// Animates the camera from its current position towards the desired offset
	/// behind the chased object. The camera's animation is controlled by a simple
	/// physical spring attached to the camera and anchored to the desired position.
	/// </summary>
	void FixedUpdate()
	{
//		Reset();

		UpdateWorldPositions();
	
		float elapsed = Time.deltaTime;

		// Calculate spring force
		Vector3 stretch = baseCamera.transform.position - desiredPosition;
		Vector3 force = -Stiffness * stretch - Damping * cameraVelocity;

		// Apply acceleration
		Vector3 acceleration = force / Mass;
		cameraVelocity += acceleration * elapsed;

		// Apply velocity
		baseCamera.transform.position += cameraVelocity * elapsed;

		UpdateMatrices();
	}
	#endregion

}
