# README #

Download and install Visual Studio 2013 and load project.  

### What is this repository for? ###

* This library is used in conjunction with the output of the flipbook animation tool.  It encompasses the playback of sprites into the Unity3D engine, and handles all collisions, triggers, and animation management for a character.  To utilize the library, the user needs to include the FlipbookLIB.dll in their Plugin folder for their Unity3D project.  A component, called flipbookplayer needs to be added to the gameobject and the flipbook asset data needs to copied into the resource folder for loading.

* Version 0.5


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Rob Anderson