﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flipbook;
using UnityEngine;

namespace FlipbookLIB
{
	public class FlipbookColliders
	{
		bool BoundOverlap(ClipBSphereData s1, Vector2 pt)
		{
			Vector2 center = new Vector2(s1.XOffset, s1.YOffset);
			int radiusSquared = s1.Radius * s1.Radius;
			float dist = (pt - center).sqrMagnitude;

			if (dist < radiusSquared)
				return true;

			return false;
		}


		/// <summary>
		/// Is the point in the bounding rectangle.  
		/// </summary>
		/// <param name="rc">bounding rectangle</param>
		/// <param name="pt">point</param>
		/// <returns>true = point is in the rectangle</returns>
		bool PointInBound(ClipBoundingData rc, Vector2 pt)
		{
			if (pt.x > rc.XOffset && pt.x < (rc.XOffset + rc.Width) && pt.y > rc.YOffset && pt.y < (rc.YOffset + rc.Height))
				return true;

			return false;
		}



		/// <summary>
		/// Detect a collision between 2 bouding rectangles
		/// </summary>
		/// <param name="r1"></param>
		/// <param name="r2"></param>
		/// <returns></returns>
		bool BoundsOverlap(ClipBoundingData r1, ClipBoundingData r2)
		{
			return !(r2.XOffset > (r1.XOffset + r1.Width)
						|| (r2.XOffset + r2.Width) < r1.XOffset
						|| r2.YOffset > (r1.YOffset + r1.Height)
						|| (r2.YOffset + r2.Height) < r1.YOffset);
		}



		/// <summary>
		/// Detect a collision between a bounding rectangle and a bounding sphere.
		/// 
		/// A circle intersects a rectangle if the centre of the circle is inside 
		/// the rectangle or if the distance from the centre of the circle to the 
		/// closest point on the border of the rectangle is less than the radius 
		/// of the circle. There are nine possibilities for the location of the 
		/// centre of the circle relative to the rectangle, depending on where it 
		/// lies relative to its extended sides:
		/// 
		///			A|B|C
		///			-+-+-
		///			D|E|F
		///			-+-+-
		///			G|H|I
		/// 
		/// In case E, clearly the circle and rectangle intersect. In cases A, C, G, 
		/// I you need to test the distance to the closest corner. In the remaining 
		/// cases use point-line distance from the centre of the circle to the nearest 
		/// edge.
		/// </summary>
		/// <param name="bboxA"></param>
		/// <param name="bSphereB"></param>
		/// <returns></returns>
		/// <seealso cref="http://stackoverflow.com/questions/15833250/collision-between-circle-shape-and-rectangle"/>
		bool BoundsOverlap(ClipBoundingData bboxA, ClipBSphereData bSphereB)
		{
			Vector2 center = new Vector2(bSphereB.XOffset, bSphereB.YOffset);
			if (PointInBound(bboxA, center) == true)
				return true;

#if false
		Vector2 topleft = new Vector2(bboxA.XOffset, bboxA.YOffset);
		Vector2 topright = new Vector2(bboxA.XOffset + bboxA.Width, bboxA.YOffset);
		Vector2 bottomleft = new Vector2(bboxA.XOffset, bboxA.YOffset + bboxA.Height);
		Vector2 bottomright = new Vector2(bboxA.XOffset + bboxA.Width, bboxA.YOffset + bboxA.Height);
#endif
			return false;
		}


		bool BoundsOverlap(ClipBSphereData bsphereA, ClipBSphereData bSphereB)
		{
			int sumRadius = bSphereB.Radius + bsphereA.Radius;
			sumRadius *= sumRadius;

			Vector2 posA = new Vector2(bsphereA.XOffset, bsphereA.YOffset);
			Vector2 posB = new Vector2(bsphereA.XOffset, bsphereA.YOffset);
			Vector2 delta = posB - posA;

			if (delta.sqrMagnitude < sumRadius)
				return true;

			return false;
		}



		/// <summary>
		/// Transform the bounding box to world space
		/// </summary>
		/// <param name="t">the transform object to calculate world position</param>
		/// <param name="bd">bounding data rectangle</param>
		/// <returns></returns>
		private ClipBoundingData TransformBoundingData(Transform t, ClipBoundingData bd)
		{
			ClipBoundingData bounds = bd;

			//		Vector3 pos = t.position;
			Vector3 offset;
			if (t.localScale.x < 0)
			{
				offset = new Vector3(-bd.XOffset, -bd.YOffset, 0);
			}
			else
			{
				offset = new Vector3(bd.XOffset, -bd.YOffset, 0);
			}
			offset = offset / 100f;

			bounds.XOffset += (int)offset.x;
			bounds.YOffset += (int)offset.y;

			return bounds;
		}






		private BaseGameEntity IsBoundsOverlapping(BaseGameEntity baseEntity, Bounds curBounds, int curID)
		{
			// loop through all of the other entities and compare against
			// their bounding boxes.
			foreach (KeyValuePair<int, GameObject> otherPair in _EntityMap)
			{
				if (otherPair.Key != curID)
				{
					GameObject other = otherPair.Value;
					BaseGameEntity otherEntity = other.GetComponent<BaseGameEntity>();

					if (baseEntity.IgnoreGameEntity(otherEntity) == false)
					{
						List<ClipBoundingData> otherBBoxes = otherEntity.BBoxes;
						if (otherBBoxes != null)
						{
							foreach (ClipBoundingData otherBBox in otherBBoxes)
							{
								if (otherBBox.Tag == "CollisionBrown")
								{
									Bounds r1 = otherEntity.ConvertBBoxToBounds(otherBBox);

									if (r1.Intersects(curBounds) == true)
									{
										Debug.Log("bounds overlapped : " + otherEntity.Name);
										return otherEntity;
									}
								}
							}
						}
					}
				}
			}
			return null;
		}


#if false
		private GameObject IsBBoxOverlapping(GameObject baseEntity, ClipBoundingData curBBox, int curID)
		{
			// loop through all of the other entities and compare against
			// their bounding boxes.
			foreach (KeyValuePair<int, GameObject> otherPair in _EntityMap)
			{
				if (otherPair.Key != curID)
				{
					GameObject other = otherPair.Value;
					BaseGameEntity otherEntity = other.GetComponent<BaseGameEntity>();

					if (baseEntity.IgnoreGameEntity(otherEntity) == false)
					{
						List<ClipBoundingData> otherBBoxes = otherEntity.BBoxes;
						if (otherBBoxes != null)
						{
							foreach (ClipBoundingData otherBBox in otherBBoxes)
							{
								if (otherBBox.Tag == "CollisionBrown")
								{
									ClipBoundingData r1 = TransformBoundingData(other.transform, otherBBox);

									if (BoundsOverlap(curBBox, r1) == true)
									{
										BoundsOverlap(curBBox, r1);
										Debug.Log("bounds overlapped : " + otherEntity.Name);
										return otherEntity;
									}
								}
							}
						}
					}
				}
			}
			return null;
		}
#endif


		public Bounds ConvertBBoxToBounds(GameObject go, ClipBoundingData bbox)
		{
			Bounds bounds = new Bounds();

			bounds = ConvertBoundingData(go.transform, bbox);

			return bounds;
		}


		/// <summary>
		/// Check if entity collided with another entity.
		/// </summary>
		/// <param name="curGameObject"></param>
		/// <param name="curID"></param>
		void CheckCollidedWith(GameObject curGameObject, int curID)
		{
			FlipbookAssets curEntity = curGameObject.GetComponent<FlipbookAssets>();

			if (curEntity.BBoxes != null)
			{
				foreach (ClipBoundingData curBBox in curEntity.BBoxes)
				{
					if (curBBox.Tag == "CollisionRed")
					{
						// we need to check this BBOX against all entities
						Bounds r1 = curEntity.ConvertBBoxToBounds(curBBox);
						BaseGameEntity overlapped = IsBoundsOverlapping(curEntity, r1, curID);

						if (overlapped != null)
						{
							overlapped.SendMessage("onHurt", curEntity);			// send message to overlapped entity (received) that has been collided with
							curEntity.SendMessage("onCollided", overlapped);		// send message to entity that triggered the collision that a collision has occurred.
						}
					}
				}
			}
		}
	}
}
