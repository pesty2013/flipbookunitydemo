﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Flipbook
{
	/// <summary>
	/// Playback options for the sequences
	/// </summary>
	public enum ePlayback
	{
		PlaybackNormal,
		PlaybackLoop,
		PlaybackPingPong,
		PlaybackReverse
	};

	public enum eColliders
	{
		ColliderNone,
		ColliderBody,
		ColliderHead,
		ColliderAttack,
		ColliderDefense,
	};


	public class ClipBoundingData
	{
		#region Flipbook
		public int XOffset = 0;
		public int YOffset = 0;
		public int Width = 0;
		public int Height = 0;
		public string Tag = string.Empty;
		public int Hashvalue = 0;
		#endregion

		#region Preprocessed Properties
		[XmlIgnore]
		public Vector3 offsetLeft;
		[XmlIgnore]
		public Vector3 offsetRight;
		[XmlIgnore]
		public eColliders ColliderTag;
		#endregion
	}


	public class ClipBSphereData
	{
		public int XOffset = 0;
		public int YOffset = 0;
		public int Radius = 0;
		public string Tag = string.Empty;
		public int Hashvalue = 0;
	}

	public class ClipBLineData
	{
		public int XStart = 0;
		public int YStart = 0;
		public int XEnd = 0;
		public int YEnd = 0;
		public string Tag = string.Empty;
		public int Hashvalue = 0;
	}




	public class ClipTriggerData
	{
		public string Tag = string.Empty;
		public int Hashvalue = 0;
		public int XOffset = 0;
		public int YOffset = 0;
	}


	/// <summary>
	/// Sprite class that holds the sprite reference, and
	/// holds the local offset values to the parent.
	/// </summary>
	public class ClipSpriteData
	{
		public string Name = string.Empty;
		public int Hashcode = 0;
		public int XOffset = 0;
		public int YOffset = 0;
	}

	/// <summary>
	/// The exported frame data that is exported
	/// over to the final game data.  This contains
	/// a list of triggers, bounding boxes, and the
	/// sprites required to build the KeyFrame.
	/// </summary>
	public class ClipFrameData
	{
		public List<ClipSpriteData> Sprites = null;
		public List<ClipBoundingData> BBoxes = null;
		public List<ClipTriggerData> Triggers = null;
		public List<ClipBSphereData> BSpheres = null;
		public List<ClipBLineData> BLines = null;
	}


	/// <summary>
	/// The exported class stores all of the clip data
	/// and the List of frame data.  This contains all
	/// of the playback information, and name of the
	/// clip.
	/// </summary>
	public class ClipData
	{
		public string Name = string.Empty;
		public ePlayback Playback = ePlayback.PlaybackNormal;
		public int NumFrames = 0;
		public int Fps = 0;
		public List<ClipFrameData> Frames;

		public ClipData()
		{
			Frames = new List<ClipFrameData>();
		}
	}


	/// <summary>
	/// Holds all of the clip data for a particular
	/// animation bank of clips.
	/// </summary>
	public class Clips
	{
		public string Name = string.Empty;
		public int NumClips = 0;
		public List<ClipData> ClipList = null;

		[XmlIgnore]
		public Dictionary<string, ClipData> ClipMap = new Dictionary<string, ClipData>();

		/// <summary>
		/// The constructor builds out the map file
		/// </summary>
		public void InitClips()
		{
			if (ClipList != null)
			{
				foreach (ClipData clip in ClipList)
				{
					ClipMap.Add(clip.Name, clip);
				}
			}
		}
	}



	// holds all of the atlas information associated with 
	// a group of animations.
	public class CelSheet
	{
		public string Name = string.Empty;
		public List<string> CelList = null;
	}


	/// <summary>
	/// </summary>
	public class CelData
	{
		public string Name = string.Empty;
		public int XOffset = 0;
		public int YOffset = 0;
		public int Width = 0;
		public int Height = 0;
	}


	/// <summary>
	/// a cel map contains the list of cels contain inside of 
	/// an sprite atlas file.
	/// </summary>
	public class CelMap
	{
		public string Name = string.Empty;
		public List<CelData> Atlas = null;
	}
}
