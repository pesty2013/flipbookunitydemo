﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Flipbook
{
	public class CelLayers : MonoBehaviour
	{
		public List<CelLayer> layers;
		public CelLayer layerPrefab;
		public bool Debug;


		void Start()
		{
			InitFlipbookLayers(4);
		}

		/// <summary>
		/// create the number of layers that the flipbook 
		/// sequence script requires.
		/// </summary>
		/// <param name="numLayers"></param>
		public void InitFlipbookLayers(int numLayers)
		{
			for (int i = 0; i < numLayers; i++)
			{
				CelLayer layer = (CelLayer)Instantiate(layerPrefab);
				layer.transform.parent = this.transform;
				layer.name = "CelLayer" + i;

				layers.Add(layer);
			}
		}


		/// <summary>
		/// Set the current sprite up
		/// </summary>
		/// <param name="layernum"></param>
		/// <param name="sprite"></param>
		/// <param name="name"></param>
		public void SetSprite(int layernum, Sprite sprite, string name, Vector3 localPos)
		{
			CelLayer layer = layers[layernum];
			layer.transform.position = Vector3.zero;
			layer.transform.localPosition = localPos;
			layer.currentSprite = sprite;
			layer.currentName = name;
		}
	}
}
